# 项目介绍

是一个较为完整的全栈教务管理系统项目 适合刚学习过Vue和NodeJs的人 或者缺少项目经验的人 通过练习该项目 可以快速提升 ElementUi 和 Vue 的使用 同时对于 NodeJs 的 Express 框架使用也有很大提升 包括 mongodb 数据库的使用等... 该项目的功能较为丰富 实际细节大家可以在查看代码时或演示功能时发现  **通过完整的练习 该项目是一个可以写到简历上的项目**

[视频辅助教程介绍地址](https://www.bilibili.com/video/BV1bB4y117NP?spm_id_from=333.999.0.0)

**该项目的页面多 功能也多 一定要动手实现一遍哦~**

接口文档在根目录下的 **api_doc.md** 中查看 ！！！

接口文档在根目录下的 **api_doc.md** 中查看 ！！！ 
 
接口文档在根目录下的 **api_doc.md** 中查看 ！！！


> 项目的容错提示并没有写的特别特别的完整 因为省时间吗~ 具体可以通过接口返回值进行判断. code === 201 && message === "success"

> **同时项目中的代码还有很多可以优化的地方 看看您是否能发现 并修改代码**~


# 项目启动

## 接口导入

项目使用 POSTMAN 进行接口的测试 将根目录下的 **jwxt.postman_collection.json** 中的集合接口导入到 POSTMAN 中 查看详细参数

**根目录下的  api_doc.md 是后台接口的简易文档 可以预览一下**

## 手动启动
  
 + yarn install
 + yarn serve
 + node server/server.js 

**记得安装数据库 开启数据库 并连接数据库！！！ 具体解释看下面数据库部分的解释**

 > 项目服务端配置文件在 /server/config/index.js

## 数据库

需要安装 Mongodb 数据库 并搭建复制集 因为用到了Mongodb的事务 Mongodb 的事务是基于复制集集群生效的

需要将 根目录下的 **db_data** 中的集合文件 导入到 数据库中生成集合结构

搭建两台即可 搭建前先把根目录下的数据库数据导入到mongodb数据库中(使用指令和可视化工具均可) 在进行复制集的搭建(搭建方式自行百度一下，在windows上即可，**不搭建复制集也可以 就是在进行支付操作时报错了不会进行回滚，为了更快速的启动项目也可以选择先不搭建复制集**)

> 本例中复制集集群开启了集群安全认证 为了减少复杂度 你们在搭建的时候可以将认证去掉(不设置开启认证 默认就是没认证的) 在使用mongoose连接时就不需要用户名和密码了

> 配置文件在 /server/config/index.js

# 技术栈

## 前台

+ Vue2 + VueRouter3 + Vuex3 + ElementUi + Axios + Socket.io (主要用于支付时获取服务器的主动推送，使用AJAX长轮询也可以实现，这里为了项目的丰富性 使用了 Socket.io)

**支付功能是站内的虚拟币支付 不是真钱 流程和真实支付类似 只不过支付是在我们自己的服务器操作的(这里只是简单实现一下，增加项目的丰富性)**

## 后台/后端的支付相关界面

NodeJs + Express + Jquery + Art-Template (后台的几个支付相关的界面)

## 数据库

Mongodb

# 功能点 知识点

大致写一下功能点(每一个功能都是前后端都要实现的 不只是前端) **还有一些细节的东西只有您写了这个项目才能体会到**~

+ 数据库的设计(10个集合) - 由于时间问题 没有画集合之间的关联关系图... 也没有做特别的细化
+ 使用到了Mongodb的高可用复制集集群+集群认证+复制集集群事务处理
+ 整体界面布局使用 ElementUi 并兼容移动端 ， 同时使用到了 百分之 60 - 70 的 ElementUi 组件
+ 登录业务 - 包含不同角色的登录 + 后台 JWT 的校验 + 路由守卫的校验 + 页面访问控制
+ 导航菜单权限 - 不同角色拥有不同的权限 高级别角色拥有低级别角色的权限
+ 搜索学生 - 首页的搜索  不知道搜索什么了 就搜学生吧 ~
+ 控制面板页面数据的同步
+ 查询交易额 - 当天/上个星期/上个月
+ 查询新增学生人数 - 当天/上个星期/上个月
+ 控制面板明星学生查询 - 总分最高的
+ 控制面板活动时间线查询
+ 学生添加 - 一共 16 个控件
+ 邮件通知 - 学生添加成功 发送邮件通知学生
+ 学生视图
+ 学生编辑
+ 学生名单 - 查询/分页/搜索/删除(假删除，标识)
+ 老师添加 - 一共 13 个控件
+ 老师视图
+ 老师编辑
+ 老师名单 - 查询/分页/搜索/删除(假删除，标识)
+ 学院列表 - 查询/分页/搜索/删除(假删除，标识)
+ 学院添加
+ 学院编辑
+ 科目列表 - 查询/分页/搜索/删除(假删除，标识)
+ 科目添加
+ 科目编辑
+ 薪资管理 - 查询/分页/搜索/删除(假删除，标识，这个删除联动 老师删除的功能)
+ 收费管理(订单管理...) - 查询/分页/搜索/删除(假删除，标识) 
+ 花费管理 - 查询/分页/搜索/删除(假删除，标识) 
+ Excel 数据表格的导出
+ 学生个人简历 - 支持导出PDF
+ 交易支付 - 支持移动端和PC端(PC端生成二维码并加上用户头像,扫码支付，移动端直接跳转到支付服务器地址) - (当然是假支付，站内的虚拟货币，默认每个学生和老师都是1000元，只是为了熟悉一下类似相关的业务，支付后台也是手动简单实现一下)
+ 支付结果查询/自动反馈 - Websocket 实现
+ 活动添加 - 定时任务的练习 - 通过添加活动在指定的时间通过批量发送邮件通知学生(这做的比较简单 没有按班级划分直接所有学生了，严格应按照班级划分 活动也是 )
+ 收件箱功能 - 含未读通知提示与发送邮件(站内邮箱)
+ 登出功能
+ 错误日志记录
+ 接口防刷中间件开发
+ ...等功能

> 上面只是将大致的功能点写到了 这里 其实还有很多细节性的处理 大家可以在代码中查看

# 页面

+ 整个项目页面为 28 个 全部使用 ElementUi 开发 项目中ElementUi的组件我们大概有用到百分之 **60 - 70**  所以练手 ElementUi 的使用也是不错的

## 页面结构

+ pages
  + Account
    + AccountCharge
      + index.vue
    + AccountSpend
      + index.vue
    + AccountTrade
      + index.vue
    index.vue
  + Actively
    + index.vue
  + ControlBoard
    + index.vue
  + Department 
    + DepartmentAdd
      + index.vue
    + DepartmentEdit
      + index.vue
    + index.vue
  + Libary
    + index.vue (未开发)
  + Login
    + index.vue
  + NotFound
    + index.vue
  + PostMan
    + index.vue
  + Profile
    + index.vue
  + Student
    + StudentAdd
      + index.vue
    + StudentEdit
      + index.vue
    + StudentView
      + index.vue
    + index.vue
   + Subject
    + SubjectAdd
      + index.vue
    + SubjectEdit
      + index.vue
    + index.vue
   + Teacher
    + TeacherAdd
      + index.vue
    + TeacherEdit
      + index.vue
    + TeacherView
      + index.vue
    + index.vue
   + TestList
    + index.vue (未开发)
   + Time
    + index.vue (未开发) 

# 组件

+ Breadcrumb
+ CustomTag
+ Footer
+ Header
+ Nav
+ PanelDesc
+ ToggleContent
