# 后台接口文档

**在项目的根目录有 POSTMAN 导出的集合文件 jwxt.postman_collection.json  给这个文件导入到POSTMAN中可以在POSTMAN中进行测试 注意URL地址**

如果你前后端都会就可以前后端都练习一下 如果你不会**NodeJs** 那么可以参考这个文档和**POSTMAN**进行**前端相关部分的开发**

**在请求接口时后台做了丰富的提示 如果不会根据提示也可以修改正确!!!**

**注意项目的用户做成了不能多地登录 所以登录了一个用户 重复登录是不可以的 具体的字段值为 loginstatus**

具体的功能参阅代码实现

## 项目地址

+ 默认情况下：http://localhost:3000

如果你修改了 那么替换成你自己的端口号即可  端口号和登录态保持时间可以在 **server/config/index.js** 修改

## 中间件

### 定时任务 - 活动发送邮件中间件

+ server/middleware/activelyEmail.js

### 防刷中间件

+ server/middleware/antibrush.js

### 认证中间件

+ server/middleware/auth.js

### 上传中间件

+ server/middleware/uploadFile.js

### 参数验证中间件

+ server/middleware/validate.js

###  多模块发送邮件中间件

+ server/middleware/sendEmail.js

## 路由文件夹

路由在 server/router/ 下查看

## token 参数

**除了请求 登录接口 不需要 携带请求头的token参数以外 其他的接口均需要携带**

token 请求头参数的值 **在登陆接口请求后就会返回**  请妥善保管  在以后的请求中携带到请求头中

## 登录模块

### 登录接口

| 请求地址   | 请求方式 | 参数类型 |
| ---------- | -------- | -------- |
| /api/login | POST     | body     |

#### 请求示例

```json
{
    "name":"Lucson",
    "pwd":"123456",
    "loginType":4
}
```

#### **请求参数**

**本项目是一个多角色的项目 所以涉及到不同角色的登录**

**管理员账号默认只有一个 Lucson 123456**

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
name | 管理员用户名 | loginType 为 4 时 必选 | Lucson
pwd | 管理员密码 | loginType 为 4 时 必选 | 123456
studentName | 学生姓名 | loginType 为 1 时 必选 | 123
studentNo | 学生学号 | loginType 为 1 时 必选 | UsEuGEYV2FbF3I1OCknvf
teacherName | 教师用户名 | loginType 为 2 时 必选 | qwe
teacherNo | 教师工号 | loginType 为 2 时 必选 | GyJOs6Y3iBZ9AxW-xLxhA
deanName | 院长用户名 | loginType 为 3 时 必选 | Lucson
deanNo | 院长工号 | loginType 为 3 时 必选 | 1
loginType | 登录类型 | 必选 | 1(学生) \| 2(教师) \| 3(院长) \|4(校长) 



#### 响应示例

```json
{
    "code": 201,
    "message": "success",
    "data": {
        "userInfo": {
            "_id": "625456f953050000bc004d23",
            "name": "Lucson",
            "role": "admin",
            "permission": 4,
            "loginStatus": 0
        },
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiTHVjc29uIiwicHdkIjoiMTIzNDU2IiwibG9naW5UeXBlIjo0LCJleHBpcmUiOjE2NTEzMTc0NjE2OTcsImlkIjoiNjI1NDU2Zjk1MzA1MDAwMGJjMDA0ZDIzIn0.f68c4cecdc258f2abea300a89b4015d6e0fe55e32114233b695137da5c5349af"
    }
}
```

> 根据登录的角色不同 返回的用户信息也不同


### 认证接口

| 请求地址  | 请求方式 | 参数类型    |
| --------- | -------- | ----------- |
| /api/auth | post     | body&header |

#### 请求示例

body

```js
{
    "loginType":4,
    "id":"625456f953050000bc004d23"
}
```

header


```js
{
    "token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiTHVjc29uIiwicHdkIjoiMTIzNDU2IiwibG9naW5UeXBlIjo0LCJleHBpcmUiOjE2NTEzMTc0NjE2OTcsImlkIjoiNjI1NDU2Zjk1MzA1MDAwMGJjMDA0ZDIzIn0.f68c4cecdc258f2abea300a89b4015d6e0fe55e32114233b695137da5c5349af"
}
```

> 下面的所有接口都需要加上 token 否则请求不通过 下面我就不再写了哦

#### **请求参数**

**本项目是一个多角色的项目 所以涉及到不同角色的登录**

**管理员账号默认只有一个 Lucson 123456**

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
id | 登录用户的ID | 必选 | 625456f953050000bc004d23  
loginType | 登录的类型 | 必选 | 1 \| 2 \| 3 \|4 


#### 响应示例


```js
{
    "code": 2,
    "message": "token 合法",
    "data": {
        "name": "Lucson",
        "pwd": "123456",
        "loginType": 4,
        "expire": 1651317461697,
        "id": "625456f953050000bc004d23"
    }
}
```

> 这里的响应结果有多种不同 code 会根据不同的 message 提示进行变化 -1 token 缺失 0 不合法  1 过期 2 合法 这四个值

### 登出接口

| 请求地址    | 请求方式 | 参数类型 |
| ----------- | -------- | -------- |
| /api/logout | get      | query    |

#### 请求示例


```
http://localhost:3000/api/logout?id=625456f953050000bc004d23&loginType=1
```


#### 请求参数


参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
id | 登录用户的ID | 必选 | 625456f953050000bc004d23  
loginType | 登录的类型 | 必选 | 1 \| 2 \| 3 \|4 

> 需要在header中加上token

#### 响应示例


```js
{
    "code": 0,
    "message": "token 已过期",
    "data": {}
}
```


## 控制面板模块

### 学生总数接口

| 请求地址           | 请求方式 | 参数类型 |
| ------------------ | -------- | -------- |
| /api/student/count | GET      | 无参数   |

#### 请求示例


```
http://localhost:3000/api/student/count
```


#### 请求参数

无参数

#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "count": 32
    }
}
```

> **这里的响应可以有多种结果 但是只要 code === 201 && message === 'success' 就代表成功 这是统一的** 下面的接口中我就不在解释了哦 在开发中也可以通过提示的错误信息明白意思

### 部门总数接口

| 请求地址              | 请求方式 | 参数类型 |
| --------------------- | -------- | -------- |
| /api/department/count | GET      | 无参数   |

#### 请求示例


```
http://localhost:3000/api/department/count
```


#### 请求参数

无参数

#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "count": 14
    }
}
```


### 总收益接口

| 请求地址               | 请求方式 | 参数类型 |
| ---------------------- | -------- | -------- |
| /api/order/total_price | GET      | 无参数   |

#### 请求示例


```
http://localhost:3000/api/order/total_price
```


#### 请求参数

无参数

#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "totalPrice": [
            {
                "_id": null,
                "totalPrice": 212216200
            }
        ]
    }
}
```


### 明星学生接口

| 请求地址                           | 请求方式 | 参数类型 |
| ---------------------------------- | -------- | -------- |
| /api/student/get_excellent_student | GET      | 无参数   |

#### 请求示例


```
http://localhost:3000/api/student/get_excellent_student	
```


#### 请求参数

无参数

#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "count": [
            {
                "_id": "625a94629fe0734be5ea6c43",
                "studentNo": "1",
                "studentName": "Lee",
                "studentPhone": "15236148260",
                "studentEmail": "2857734156@qq.com",
                "studentSex": 1,
                "studentBirthday": 2312312356456,
                "studentInSchoolDate": 123456789,
                "studentAvatar": "http://localhost:3000/uploads/student/12843.402566686013.jpeg",
                "studentDepartment": "1",
                "studentNowAddress": "暂无地址",
                "studentHomeAddress": "暂无地址",
                "studentParentName": "未填写",
                "studentParentPhone": "15236148260",
                "studentParentJob": "未填写",
                "studentSkill": [
                    "\"html\",\"css\""
                ],
                "studentTotalScore": 1499,
                "studentRecommend": "未填写",
                "studentSubjectsIds": "1,2,3,4",
                "dollar": 1000,
                "isDelete": 0,
                "permission": 1,
                "role": "student",
                "studentGradeId": 1,
                "loginStatus": 0,
                "__v": 0
            }
        ]
    }
}
```

> 该接口会返回指定的条数 后台自动筛选分数最高的同学  字段有点多但是其实有些功能我们本项目中没实现

### 最新活动接口

| 请求地址                        | 请求方式 | 参数类型 |
| ------------------------------- | -------- | -------- |
| /api/actively/get_news_actively | GET      | 无参数   |

#### 请求示例


```
http://localhost:3000/api/actively/get_news_actively
```


#### 请求参数

无参数

#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "activelys": [
            {
                "_id": "626bbcee0b45c0dad10a710d",
                "activelyNo": "Cni9Kgax0Iwx-oCy2QhCr",
                "activelyTitle": "sad",
                "activelyDate": 1651190400000,
                "isDelete": "0",
                "__v": 0
            },
            {
                "_id": "626bbd2d0b45c0dad10a7117",
                "activelyNo": "C2bqqiR7_SHWJGhRkfrSh",
                "activelyTitle": "乌拉",
                "activelyDate": 1648771200000,
                "isDelete": "0",
                "__v": 0
            }
        ]
    }
}
```
> 该接口会返回指定的条数 后台自动筛选

### 交易额查询接口

| 请求地址                     | 请求方式 | 参数类型 |
| ---------------------------- | -------- | -------- |
| /api/order/cycle_trade_price | GET      | query    |

#### 请求示例


```
http://localhost:3000//api/order/cycle_trade_price?cycleType=nowDay
```


#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
cycleType | 周期类型 | 必选 | nowDay(今天) \| lastWeek(上星期) \| lastMonth(上个月)  


#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "tradePrice": [
            {
                "_id": null,
                "cycleTradePrice": 212200588
            }
        ]
    }
}
```


### 新增学生数量接口

| 请求地址                     | 请求方式 | 参数类型 |
| ---------------------------- | -------- | -------- |
| /api/student/new_add_student | GET      | query    |

#### 请求示例


```
http://localhost:3000/api/student/new_add_student
```


#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
cycleType | 周期类型 | 必选 | nowDay(今天) \| lastWeek(上星期) \| lastMonth(上个月)  

#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "count": 2
    }
}
```


## 学生模块

### 获取学生名单接口

| 请求地址                       | 请求方式 | 参数类型 |
| ------------------------------ | -------- | -------- |
| /api/student/get_student_infos | GET      | query    |

#### 请求示例


```
http://localhost:3000/api/student/get_student_infos?searchType=1&searchContent=1&pageIndex=1&pageSize=6
```


#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
searchType | 搜索类型 | 可选 | 可选值 1 和 2   1(代表使用学号查询)  2(代表使用姓名查询)
searchContent | 搜索内容 | 可选 | 666 
pageIndex | 当前页 | 可选 | 1
pageSize | 页大小 | 可选 | 12  

> 以上参数均可不传递 默认进入页面时应该设么也不传 后台都设有默认值的处理

#### 响应示例


```
{
    "code": 201,
    "message": "success",
    "data": {
        "studens": [
            {
                "_id": "626ad3470ba635e867d51546",
                "studentNo": "X7U3Ab1J8JwKadqKPPtRs",
                "studentName": "qwe",
                "studentPhone": "15236148260",
                "studentEmail": "2857734156@qq.com",
                "studentSex": 0,
                "studentBirthday": 1650988800000,
                "studentInSchoolDate": 1651161600000,
                "studentAvatar": "",
                "studentDepartment": "1",
                "studentNowAddress": "asd",
                "studentHomeAddress": "asd",
                "studentParentName": "asd",
                "studentParentPhone": "15236148260",
                "studentParentJob": "asd",
                "studentSkill": [
                    ""
                ],
                "studentTotalScore": 7,
                "studentRecommend": "",
                "dollar": 1000,
                "isDelete": 0,
                "permission": 1,
                "role": "student",
                "loginStatus": 0,
                "__v": 0
            }
        ]
    }
}
```

> 实际根据搜索内容返回 没搜索则默认返回12条第一页数据  该接口没做搜索总数的获取 所以模糊搜索分页的页码数还是和不搜索一样...

### 删除学生接口

| 请求地址                    | 请求方式 | 参数类型 |
| --------------------------- | -------- | -------- |
| /api/student/delete_student | DELETE   | query    |

#### 请求示例


```
http://localhost:3000/api/student/delete_student?id=62545fda53050000bc004d24
```


#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
id | 学生ID | 必选 | 62552b4d76000000ba0006e8 

#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {}
}
```


### 获取单个学生信息接口

| 请求地址                 | 请求方式 | 参数类型 |
| ------------------------ | -------- | -------- |
| /api/student/get_student | GET      | query    |

#### 请求示例


```
http://localhost:3000/api/student/get_student?id=62552a7376000000ba0006e4
```


#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
id | 学生ID | 必选 | 62552b4d76000000ba0006e8 

#### 响应示例

### 添加学生接口

| 请求地址                 | 请求方式 | 参数类型 |
| ------------------------ | -------- | -------- |
| /api/student/add_student | POST     | body     |

#### 请求示例


```js
{
    studentNo: "-EnNVVWtuqzfczygyXWA1"
    studentName: "测试"
    studentSkill: "789,456"
    studentPhone: 15236148260
    studentEmail: "2857734156@qq.com"
    studentSex: 0
    studentBirthday: 1650384000000
    studentInSchoolDate: 1649779860000
    studentDepartment: 1
    studentNowAddress: "qwe"
    studentHomeAddress: "wqe"
    studentParentName: "qwe"
    studentParentPhone: 15365148260
    studentParentJob: 123
    studentRecommend: 
    studentAvatar: 
}
```


#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
studentNo | 学号 | 必选 | UsEuGEYV2FbF3I1OCknvf  
studentName | 学生姓名 | 必选 | Lucson 
studentSkill | 技能 | 必选 | html,css,js  [npm 搜索  vue2-tags 使用这个组件]
studentPhone | 电话 | 必选 | 15236148261  
studentEmail | 邮箱 | 必选 | 2857734156@qq.com  
studentSex | 学生性别 | 必选 | 0(男) 1(女) 
studentBirthday | 生日 | 必选 | 123456456467(时间戳) 
studentInSchoolDate | 入学时间 | 必选 | 4564658751234 (时间戳) 
studentDepartment | 学院ID | 必选 | 1 
studentNowAddress | 现居住地址 | 必选 | 河南 
studentHomeAddress | 永久居住地址 | 必选 | 河南 
studentParentName | 父母姓名 | 必选 | 测试姓名  
studentParentPhone | 父母电话 | 必选 | 15236148261  
studentParentJob | 父母职业 | 必选 | 资本家 
studentRecommend | 个人介绍 | 可选 | 你好呀！！ 
studentAvatar | 头像 | 可选 | File 对象


#### 响应示例


```js
{
	"code": 201,
	"message": "success",
	"data": {
		"studentNo": "-EnNVVWtuqzfczygyXWA1",
		"studentName": "测试",
		"studentPhone": "15236148260",
		"studentEmail": "2857734156@qq.com",
		"studentSex": 0,
		"studentBirthday": 1650384000000,
		"studentInSchoolDate": 1649779860000,
		"studentAvatar": "",
		"studentDepartment": "1",
		"studentNowAddress": "qwe",
		"studentHomeAddress": "wqe",
		"studentParentName": "qwe",
		"studentParentPhone": "15365148260",
		"studentParentJob": "123",
		"studentSkill": ["789,456"],
		"studentTotalScore": 1404,
		"studentRecommend": "",
		"dollar": 1000,
		"isDelete": 0,
		"permission": 1,
		"role": "student",
		"loginStatus": 0,
		"_id": "626bdab34be4dfde69663a05",
		"__v": 0
	}
}
```


### 修改学生接口

| 请求地址                    | 请求方式 | 参数类型 |
| --------------------------- | -------- | -------- |
| /api/student/update_student | PUT      | body     |

#### 请求示例


```js
{
    _id:"456asd8as7dasd33as12as1"
    studentNo: "-EnNVVWtuqzfczygyXWA1"
    studentName: "测试"
    studentSkill: "789,456"
    studentPhone: 15236148260
    studentEmail: "2857734156@qq.com"
    studentSex: 0
    studentBirthday: 1650384000000
    studentInSchoolDate: 1649779860000
    studentDepartment: 1
    studentNowAddress: "qwe"
    studentHomeAddress: "wqe"
    studentParentName: "qwe"
    studentParentPhone: 15365148260
    studentParentJob: 123
    studentRecommend: 
    studentAvatar: 
}
```


#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
_id | ID | 必选 | 12S3ad5A4D65DA5d46  
studentNo | 学号 | 可选 | UsEuGEYV2FbF3I1OCknvf  
studentName | 学生姓名 | 可选 | Lucson 
studentSkill | 技能 | 可选 | html,css,js  [npm 搜索  vue2-tags 使用这个组件]
studentPhone | 电话 | 可选 | 15236148261  
studentEmail | 邮箱 | 可选 | 2857734156@qq.com  
studentSex | 学生性别 | 可选 | 0(男) 1(女) 
studentBirthday | 生日 | 可选 | 123456456467(时间戳) 
studentInSchoolDate | 入学时间 | 可选 | 4564658751234 (时间戳) 
studentDepartment | 学院ID | 可选 | 1 
studentNowAddress | 现居住地址 | 可选 | 河南 
studentHomeAddress | 永久居住地址 | 可选 | 河南 
studentParentName | 父母姓名 | 可选 | 测试姓名  
studentParentPhone | 父母电话 | 可选 | 15236148261  
studentParentJob | 父母职业 | 可选 | 资本家 
studentRecommend | 个人介绍 | 可选 | 你好呀！！ 
studentAvatar | 头像 | 可选 | File 对象


#### 响应示例


```js
{
    "code": 201,
    "message": "要修改的数据和已有的数据相同",
    "data": {
        "acknowledged": true,
        "modifiedCount": 0,
        "upsertedId": null,
        "upsertedCount": 0,
        "matchedCount": 1
    }
}
```

> 如果修改成功 modifiedCount 就是 1


### 根据学号模糊搜索学生接口

| 请求地址                       | 请求方式 | 参数类型 |
| ------------------------------ | -------- | -------- |
| /api/student/student | GET      | query    |


#### 请求示例


```
http://localhost:3000/api/student/student?studentNo=666
```

#### 请求参数


参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
studentNo | 学号 | 必选 | 666

#### 响应示例


```js
{
    "code": 400,
    "message": "success",
    "data": {
        "student": [
            {
                "_id": "625ae56b56493d62e7ed6704",
                "studentNo": "666",
                "studentName": "Lee",
                "studentPhone": "15236148260",
                "studentEmail": "2857734156@qq.com",
                "studentSex": 1,
                "studentBirthday": 2312312356456,
                "studentInSchoolDate": 0,
                "studentAvatar": "http://localhost:3000/uploads/student/7000.2715612644615.jpeg",
                "studentDepartment": "1",
                "studentNowAddress": "暂无地址",
                "studentHomeAddress": "暂无地址",
                "studentParentName": "未填写",
                "studentParentPhone": "15236148260",
                "studentParentJob": "未填写",
                "studentSkill": [
                    "\"html\",\"css\""
                ],
                "studentTotalScore": 735,
                "studentRecommend": "未填写",
                "studentSubjectsIds": "1,2,3,4",
                "dollar": 1000,
                "isDelete": 0,
                "permission": 1,
                "role": "student",
                "studentGradeId": 1,
                "loginStatus": 0,
                "__v": 0
            }
        ]
    }
}
```


## 老师模块

### 老师列表接口

| 请求地址                       | 请求方式 | 参数类型 |
| ------------------------------ | -------- | -------- |
| /api/teacher/get_teacher_infos | GET      | query    |

#### 请求示例



```
http://localhost:3000/api/teacher/get_teacher_infos
```



#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
searchType | 搜索类型 | 可选 | 可选值 1 和 2   1(代表使用工号查询)  2(代表使用姓名查询)
searchContent | 搜索内容 | 可选 | 666 
pageIndex | 当前页 | 可选 | 1
pageSize | 页大小 | 可选 | 12  


> 以上参数均可不传递 默认进入页面时应该设么也不传 后台都设有默认值的处理

#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "teacher": [
            {
                "_id": "6254652253050000bc004d25",
                "teacherNo": "1",
                "teacherName": "Lucson Li",
                "teacherPhone": "15236148260",
                "teacherEmail": "2857734156@qq.com",
                "teacherSex": 1,
                "teacherBirthday": 1649698082825,
                "teacherInSchoolDate": 1649698082825,
                "teacherAvatar": "http://192.168.0.105:3000/uploads/teacher/76963.704079757.png",
                "teacherDepartment": 1,
                "teacherNowAddress": "河南郑州",
                "teacherHomeAddress": "河南洛阳",
                "teacherRecommend": "热爱学习 热爱进步 渴望改变自己的人生！",
                "teacherSalary": 10000,
                "teacherExperience": 1,
                "position": 1,
                "loginStatus": 0,
                "lastLoginDate": 1649698082825,
                "dollar": 1000,
                "isDelete": 0,
                "permission": 2,
                "role": "teacher"
            },
            {
                "_id": "625d144a641fe5ac89440f2e",
                "teacherNo": "666",
                "teacherName": "asd",
                "teacherPhone": "15236148260",
                "teacherEmail": "2857734156@qq.com",
                "teacherSex": 1,
                "teacherBirthday": 123231,
                "teacherSubjectId": "",
                "teacherInSchoolDate": 12313132,
                "teacherAvatar": "http://localhost:3000/uploads/teacher/276061.5170811022.png",
                "teacherDepartment": 1,
                "teacherNowAddress": "未填写",
                "teacherHomeAddress": "未填写",
                "teacherSkill": "1,2",
                "teacherRecommend": "未填写",
                "teacherSalary": 12500,
                "teacherExperience": 1,
                "position": 1,
                "loginStatus": 0,
                "gradeIds": "1",
                "lastLoginDate": 0,
                "dollar": 1000,
                "isDelete": 1,
                "permission": 2,
                "role": "teacher",
                "__v": 0
            },
            {
                "_id": "625d16b00b0af1b800d6a017",
                "teacherNo": "666",
                "teacherName": "asd",
                "teacherPhone": "15236148260",
                "teacherEmail": "2857734156@qq.com",
                "teacherSex": 1,
                "teacherBirthday": 123231,
                "teacherSubjectId": "",
                "teacherInSchoolDate": 12313132,
                "teacherAvatar": "http://localhost:3000/uploads/teacher/83190.49166992553.png",
                "teacherDepartment": 1,
                "teacherNowAddress": "未填写",
                "teacherHomeAddress": "未填写",
                "teacherSkill": "1,2",
                "teacherRecommend": "未填写",
                "teacherSalary": 12500,
                "teacherExperience": 1,
                "position": 1,
                "loginStatus": 0,
                "gradeIds": "1",
                "lastLoginDate": 0,
                "dollar": 1000,
                "isDelete": 0,
                "permission": 2,
                "role": "teacher",
                "__v": 0
            }
        ]
    }
}
```

### 老师视图接口

| 请求地址                 | 请求方式 | 参数类型 |
| ------------------------ | -------- | -------- |
| /api/teacher/get_teacher | GET      | query    |

#### 请求示例


```
http://localhost:3000/api/teacher/get_teacher?id=6254652253050000bc004d25
```


#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
id | 教师ID | 必选 | 626ad2c80ba635e867d51537


#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "teacher": {
            "_id": "626a89b110becde5731382f9",
            "teacherNo": "GyJOs6Y3iBZ9AxW-xLxhA",
            "teacherName": "qwe",
            "teacherPhone": "15236148260",
            "teacherEmail": "2857734156@qq.com",
            "teacherSex": 1,
            "teacherBirthday": 1650384000000,
            "teacherSubjectId": "",
            "teacherAvatar": "http://q4.qlogo.cn/headimg_dl?dst_uin=2857734156&spec=100",
            "teacherNowAddress": "qwe",
            "teacherHomeAddress": "qwe",
            "teacherSkill": "sd",
            "teacherRecommend": "未填写",
            "teacherSalary": 200,
            "teacherExperience": 1,
            "position": 1,
            "loginStatus": 0,
            "gradeIds": "",
            "lastLoginDate": 0,
            "dollar": 1000,
            "isDelete": 0,
            "permission": 2,
            "role": "teacher",
            "__v": 0
        }
    }
}
```


### 老师添加接口

| 请求地址                 | 请求方式 | 参数类型 |
| ------------------------ | -------- | -------- |
| /api/teacher/add_teacher | POST     | body     |

#### 请求示例


```
{
    teacherNo: "OvFVw_lem2aemQQEwKX-o"
    teacherName: "cs"
    teacherPhone: 15236148260
    teacherEmail: 28577341562@qq.com
    teacherSalary: 2000
    teacherSex: 0
    teacherBirthday: 1650988800000
    teacherExperience: 1
    teacherSkill: "asd,sadasd"
    teacherNowAddress: "qwe"
    teacherHomeAddress: "wqe"
    teacherAvatar: (binary)
}
```


#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
teacherNo | 教师工号 | 必选 | TknIw7v-1ugFCX_Nii356
teacherName | 教师姓名 | 必选 | Lucson 
teacherPhone | 教师电话 | 必选 | 15236148260
teacherEmail | 教师邮件 | 必选 | 123456789@qq.com  
teacherSalary | 教师薪资 | 必选 | 10000
teacherBirthday | 教师生日 | 必选 | 465465789(时间戳) 
teacherExperience | 教师经验 | 必选 | 1 | 2 | 3 (这个自己设置 没有写数据库)
teacherSkill | 教师节能 | 可选 | HTML JS CSS  
teacherNowAddress | 现居住地址 | 必选 | 河南 
teacherHomeAddress | 永久居住地址 | 必选 | 河南
teacherAvatar | 教师头像 | 可选 | File 对象 

#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "teacherNo": "666",
        "teacherName": "asd",
        "teacherPhone": "15236148260",
        "teacherEmail": "2857734156@qq.com",
        "teacherSex": 1,
        "teacherBirthday": 123231,
        "teacherSubjectId": "",
        "teacherInSchoolDate": 12313132,
        "teacherAvatar": "http://192.168.0.105:3000/uploads/teacher/16188.58899153562.png",
        "teacherDepartment": 1,
        "teacherNowAddress": "未填写",
        "teacherHomeAddress": "未填写",
        "teacherSkill": "1,2",
        "teacherRecommend": "未填写",
        "teacherSalary": 12500,
        "teacherExperience": 1,
        "position": 1,
        "loginStatus": 0,
        "gradeIds": "1",
        "lastLoginDate": 0,
        "dollar": 1000,
        "isDelete": 0,
        "permission": 2,
        "role": "teacher",
        "_id": "626befcefd25dd774b7174f9",
        "__v": 0
    }
}
```


### 老师编辑接口

| 请求地址                    | 请求方式 | 参数类型 |
| --------------------------- | -------- | -------- |
| /api/teacher/update_teacher | PUT      | body     |

#### 请求示例


```js
{
    _id:"56as4d56sa4cx3z123as564d"
    teacherNo: "OvFVw_lem2aemQQEwKX-o"
    teacherName: "cs"
    teacherPhone: 15236148260
    teacherEmail: 28577341562@qq.com
    teacherSalary: 2000
    teacherSex: 0
    teacherBirthday: 1650988800000
    teacherExperience: 1
    teacherSkill: "asd,sadasd"
    teacherNowAddress: "qwe"
    teacherHomeAddress: "wqe"
    teacherAvatar: (binary)
}
```


#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
_id | ID | 必选 |  626a49e3f593650c3eee0a41
teacherName | 教师姓名 | 可选 | Lucson 
teacherPhone | 教师电话 | 可选 | 15236148260
teacherEmail | 教师邮件 | 可选 | 123456789@qq.com  
teacherSalary | 教师薪资 | 可选 | 10000
teacherBirthday | 教师生日 | 可选 | 465465789(时间戳) 
teacherExperience | 教师经验 | 可选 | 1 | 2 | 3 (这个自己设置 没有写数据库)
teacherSkill | 教师节能 | 可选 | HTML JS CSS  
teacherNowAddress | 现居住地址 | 可选 | 河南 
teacherHomeAddress | 永久居住地址 | 可选 | 河南
teacherAvatar | 教师头像 | 可选 | File 对象 

#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "acknowledged": true,
        "modifiedCount": 1,
        "upsertedId": null,
        "upsertedCount": 0,
        "matchedCount": 1
    }
}
```


### 删除老师接口

| 请求地址                    | 请求方式 | 参数类型 |
| --------------------------- | -------- | -------- |
| /api/teacher/delete_teacher | DELETE   | query    |

#### 请求示例


```
http://localhost:3000/api/teacher/delete_teacher?id=625d144a641fe5ac89440f2e
```


#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
id | ID | 必选 |  626a49e3f593650c3eee0a41

#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {}
}
```


## 学院模块

### 学院列表接口

| 请求地址                        | 请求方式 | 参数类型 |
| ------------------------------- | -------- | -------- |
| /api/department/get_departments | GET      | query    |

#### 请求示例


```
http://localhost:3000/api/department/get_departments?searchContent=算
```


#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
searchContent | 搜索学院名称 | 可选 | 计算机 
pageIndex | 当前页 | 可选 | 1
pageSize | 页大小 | 可选 | 12  

#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "departments": [
            {
                "_id": "625f6f32e7fa08d07d6da01a",
                "departmentNo": "666",
                "departmentName": "计算机学院",
                "departmentLeader": "2",
                "departmentCreateDate": 1649698519100,
                "isDelete": 0,
                "__v": 0,
                "studentNumber": 500,
                "dean": [ // 院长
                    {
                        "_id": "625a7c68ea01312ddba5dfa9",
                        "deanNo": "2",
                        "deanName": "pop",
                        "permission": 3,
                        "role": "dean",
                        "isDelete": 0,
                        "__v": 0,
                        "loginStatus": 0
                    }
                ]
            },
            {
                "_id": "625f6f36e7fa08d07d6da01c",
                "departmentNo": "666",
                "departmentName": "计算机学院",
                "departmentLeader": "1",
                "departmentCreateDate": 1649698519100,
                "isDelete": 0,
                "__v": 0,
                "studentNumber": 1000,
                "dean": [
                    {
                        "_id": "62552b4d76000000ba0006e8",
                        "deanNo": "1",
                        "deanName": "Lucson",
                        "permission": 3,
                        "role": "dean",
                        "isDelete": 0,
                        "loginStatus": 1
                    }
                ]
            }
        ]
    }
}
```


### 学院添加接口

| 请求地址                       | 请求方式 | 参数类型 |
| ------------------------------ | -------- | -------- |
| /api/department/add_department | POST     | body     |

#### 请求示例


```js
{
    departmentCreateDate: 1650297600000
    departmentLeader: "1"
    departmentName: "666"
    departmentNo: "k9M0oesW0ZOiy6DHg-wlS"
    studentNumber: "2000"
}
```


#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
departmentCreateDate | 创建日期 | 必选 |  123465789(时间戳)
departmentLeader | 院长ID | 必选 |  626a49e3f593650c3eee0a41
departmentName | 学院名 | 必选 |  计算机学院
departmentNo | 学院编号 | 必选 |  C5vbzJCrXrDmSXTbyjbBO
studentNumber | 学生数量 | 必选 |  2000

#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "departmentNo": "666",
        "departmentName": "计算机学院",
        "departmentLeader": "1",
        "departmentCreateDate": 2314654656798898,
        "studentNumber": 2000,
        "isDelete": 0,
        "_id": "626bf2b6fd25dd774b717511",
        "__v": 0
    }
}
```


### 学院编辑接口

| 请求地址                          | 请求方式 | 参数类型 |
| --------------------------------- | -------- | -------- |
| /api/department/update_department | PUT      | body     |

#### 请求示例

```js
{
    _id:"123asd456asd46a7d98"
    departmentCreateDate: 1650297600000
    departmentLeader: "1"
    departmentName: "666"
    departmentNo: "k9M0oesW0ZOiy6DHg-wlS"
    studentNumber: "2000"
}
```

#### 请求参数


参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
 _id | ID | 必选 |  12sd3as4d65as12zcad
departmentCreateDate | 创建日期 | 可选 |  123465789(时间戳)
departmentLeader | 院长ID | 可选 |  626a49e3f593650c3eee0a41
departmentName | 学院名 | 可选 |  计算机学院
departmentNo | 学院编号 | 可选 |  C5vbzJCrXrDmSXTbyjbBO
studentNumber | 学生数量 | 可选 |  2000

#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "acknowledged": true,
        "modifiedCount": 1,
        "upsertedId": null,
        "upsertedCount": 0,
        "matchedCount": 1
    }
}
```


### 学院删除接口

| 请求地址                          | 请求方式 | 参数类型 |
| --------------------------------- | -------- | -------- |
| /api/department/delete_department | DELETE   | query    |

#### 请求示例


```
http://localhost:3000/api/department/delete_department?id=62552afb76000000ba0006e6
```


#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
 id | ID | 必选 |  12sd3as4d65as12zcad
 
#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {}
}
```


### 获取院长接口

| 请求地址            | 请求方式 | 参数类型 |
| ------------------- | -------- | -------- |
| /api/dean/get_deans | GET      | 无参数   |

#### 请求示例


```
http://localhost:3000/api/dean/get_deans
```


#### 请求参数

无参数

#### 响应示例


```
{
    "code": 201,
    "message": "success",
    "data": {
        "deans": [
            {
                "_id": "62552b4d76000000ba0006e8",
                "deanNo": "1",
                "deanName": "Lucson",
                "permission": 3,
                "role": "dean",
                "isDelete": 0,
                "loginStatus": 1
            },
            {
                "_id": "625a7c68ea01312ddba5dfa9",
                "deanNo": "2",
                "deanName": "pop",
                "permission": 3,
                "role": "dean",
                "isDelete": 0,
                "__v": 0,
                "loginStatus": 0
            }
        ]
    }
}
```


### 获取单个学院信息接口

| 请求地址            | 请求方式 | 参数类型 |
| ------------------- | -------- | -------- |
| /api/department/get_department | GET      | query   |

#### 请求示例


```
http://localhost:3000/api/department/get_department?id=62552b0476000000ba0006e7
```


#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
 id | ID | 必选 |  12sd3as4d65as12zcad

#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "_id": "62552b0476000000ba0006e7",
        "departmentNo": "2",
        "departmentName": "金融学院",
        "departmentLeader": 1,
        "departmentCreateDate": 1649698519100,
        "isDelete": 0,
        "studentNumber": 1000
    }
}
```


## 科目模块

### 科目列表接口

| 请求地址                       | 请求方式 | 参数类型 |
| ------------------------------ | -------- | -------- |
| /api/subject/get_teacher_infos | GET      | query    |

#### 请求示例


```
http://localhost:3000/api/subject/get_teacher_infos
```


#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
searchContent | 搜索科目名称 | 可选 | SpringCloud 
pageIndex | 当前页 | 可选 | 1
pageSize | 页大小 | 可选 | 12  

#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "subjects": [
            {
                "_id": "626a5bda1d1b0000d80000e3",
                "subjectNo": "1",
                "subjectName": "SpringCloud",
                "isDelete": 0
            },
            {
                "_id": "626a5be91d1b0000d80000e4",
                "subjectNo": "2",
                "isDelete": 0,
                "subjectName": "Java"
            }
        ]
    }
}
```


### 科目添加接口

| 请求地址                 | 请求方式 | 参数类型 |
| ------------------------ | -------- | -------- |
| /api/subject/add_subject | POST     | body     |

#### 请求示例


```
{
    subjectNo:"",
    subjectName:"",
}
```


#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
subjectNo | 科目编号 | 必选 |  12sd3as4d65as12zcad
subjectName | 科目名称 | 必选 | 1

#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "subjectNo": "1",
        "subjectName": "123",
        "isDelete": 0,
        "_id": "626bfb81fd25dd774b717517",
        "__v": 0
    }
}
```


###  科目编辑接口

| 请求地址                    | 请求方式 | 参数类型 |
| --------------------------- | -------- | -------- |
| /api/subject/update_subject | PUT      | body     |

#### 请求示例


```
 {
     _id:"",
     subjectNo:"",
     subjectName:"",
 }
```


#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
_id | ID | 必选 | 4as56da8w6e3asd13as
subjectNo | 科目编号 | 可选 |  12sd3as4d65as12zcad
subjectName | 科目名称 | 可选 | 1

#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "acknowledged": true,
        "modifiedCount": 1,
        "upsertedId": null,
        "upsertedCount": 0,
        "matchedCount": 1
    }
}
```

### 科目删除接口

| 请求地址                    | 请求方式 | 参数类型 |
| --------------------------- | -------- | -------- |
| /api/subject/delete_subject | DELETE   | query    |

#### 请求示例


```
http://localhost:3000/api/subject/delete_subject?id=62552c7d76000000ba0006ec
```



#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
_id | ID | 必选 | 4as56da8w6e3asd13as


#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {}
}
```

### 获取科目总数接口

| 请求地址                    | 请求方式 | 参数类型 |
| --------------------------- | -------- | -------- |
| /api/subject/get_subject_count | GET   | query    |

#### 请求示例


```
http://localhost:3000/api/subject/get_subject_count
```


#### 请求参数

无参数

#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "count": 2
    }
}
```

### 获取单个科目接口


| 请求地址                    | 请求方式 | 参数类型 |
| --------------------------- | -------- | -------- |
| /api/subject/get_subject | GET   | query    |

#### 请求示例


```
http://localhost:3000/api/subject/get_subject?id=626a5be91d1b0000d80000e4
```

#### 请求参数


参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
 id | ID | 必选 | 4as56da8w6e3asd13as


#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "_id": "626a5be91d1b0000d80000e4",
        "subjectNo": "2",
        "isDelete": 0,
        "subjectName": "Java"
    }
}
```


## 账户模块

### 订单列表接口

| 请求地址              | 请求方式 | 参数类型 |
| --------------------- | -------- | -------- |
| /api/order/get_orders | GET      | query    |

#### 请求示例


```
http://localhost:3000/api/order/get_orders?searchContent=3&pageIndex=1&pageSize=6
```


#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
searchContent | 搜索订单号 | 可选 | SpringCloud 
pageIndex | 当前页 | 可选 | 1
pageSize | 页大小 | 可选 | 12  


#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "order": [
            {
                "_id": "62622c8f1f86eddbd5351e53",
                "orderNo": "10000000000666",
                "toId": "62552a7376000000ba0006e4",
                "toName": "Tom",
                "fromId": "6254652253050000bc004d25",
                "fromName": "Lily",
                "tradeType": 1,
                "tradeMoney": 5000,
                "tradeCreateDate": 1650601103882,
                "tradeOutDate": 1650687503882,
                "tradePayStatus": 0,
                "tradeDesc": "测试转账",
                "isDelete": 0,
                "__v": 0
            },
            {
                "_id": "62622b3fb75cdd3eb777a1a1",
                "orderNo": "10000000000666",
                "toId": "62552a7376000000ba0006e4",
                "toName": "Tom",
                "fromId": "62552a9776000000ba0006e5",
                "fromName": "Lily",
                "tradeType": 1,
                "tradeMoney": 5000,
                "tradeCreateDate": 1650600767396,
                "tradeOutDate": 1650687167396,
                "tradePayStatus": 0,
                "tradeDesc": "测试转账",
                "isDelete": 0,
                "__v": 0
            }
        ]
    }
}
```


### 订单删除接口

| 请求地址                | 请求方式 | 参数类型 |
| ----------------------- | -------- | -------- |
| /api/order/delete_order | DELETE   | query    |

#### 请求示例


```
http://localhost:3000/api/order/delete_order?id=6254682153050000bc004d28
```


#### 请求参数

| 请求地址                    | 请求方式 | 参数类型 |
| --------------------------- | -------- | -------- |
| /api/subject/get_subject | GET   | query    |


#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {}
}
```


### 创建订单接口

| 请求地址             | 请求方式 | 参数类型 |
| -------------------- | -------- | -------- |
| /api/order/add_order | POST     | body     |

#### 请求示例


```
{
    orderNo:"1",
    toId:"1",
    toName:"1",
    fromId:"1",
    fromName:"1",
    tradeType:"1",
    tradeMoney:"1",
    tradeDesc:"1"
}
```

> 以上仅为示例

#### 请求参数


参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
orderNo | 订单号 | 必选 | zpQQt4tU72zn8cD1__jsZ 
toId | 收款人ID | 必选 | 1231as2d456a
toName | 收款人姓名 | 必选 | Lucson 
fromId | 付款人ID | 必选 | 12as1a2daDA1D2 
fromName | 付款人姓名 | 必选 | Tom
tradeType | 交易类型 | 必选 | 1 \| 2 （这个看自己设置 也没有设置数据库）
tradeMoney | 交易金额 | 必选 | 10000 
tradeDesc | 交易描述 | 必选 | 测试订单


#### 响应示例


```
{
    "code": 201,
    "message": "success",
    "data": {
        "orderNo": "1",
        "id": "626c0028fd25dd774b717522"
    }
}
```


### 请求支付接口

| 请求地址 | 请求方式 | 参数类型 |
| -------- | -------- | -------- |
| /api/pay | GET      | query    |

#### 请求示例

PC 端扫码 移动端直接跳转 自行判断 可参考项目代码

```
http://localhost:3000/api/pay?orderNo=1&return_url=http://192.168.0.105:3000/api/pay/return_url&notify_url=http://192.168.0.105:3000/api/pay/notify_url
```


#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
orderNo | 订单号 | 必选 | zpQQt4tU72zn8cD1__jsZ 
return_url | 同步通知地址 | 必选 | http://192.168.0.105:3000/api/pay/return_url （这里的地址就填写这个地址就可以了 不要修改 修改了就会跳转到你自己指定的界面）
notify_url | 异步通知地址 | 必选 | http://192.168.0.105:3000/api/pay/notify_url（这里虽然传递了这个参数 但是实际没用到 因为我们不是提供给别人用的 而是自己实现的一个支付功能 如果要提供给别人用  那么我们会通过请求这个地址告诉使用者是否支付成功返回一些信息）


#### 响应示例

这里的一些返回都是界面 在服务端操作的支付界面 我们提供的支付服务 肯定要在服务器进行才安全一些 

> 这里是我们自己实现的支付服务 所以我们要自己从服务器推送消息到客户端 所以需要使用 websocket 项目介绍里都有解释

**PC端扫码  /  移动端直接跳转**

### 订单查询接口

| 请求地址             | 请求方式 | 参数类型 |
| -------------------- | -------- | -------- |
| /api/order/get_order | GET      | query    |

#### 请求示例


```
http://localhost:3000/api/order/get_order?orderNo=10000000000666
```


#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
orderNo | 订单号 | 必选 | zpQQt4tU72zn8cD1__jsZ 

#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "order": [
            {
                "_id": "62622c8f1f86eddbd5351e53",
                "orderNo": "10000000000666",
                "toId": "62552a7376000000ba0006e4",
                "toName": "Tom",
                "fromId": "6254652253050000bc004d25",
                "fromName": "Lily",
                "tradeType": 1,
                "tradeMoney": 5000,
                "tradeCreateDate": 1650601103882,
                "tradeOutDate": 1650687503882,
                "tradePayStatus": 0, // 订单状态  0 未支付 1 已过期 2 已支付
                "tradeDesc": "测试转账",
                "isDelete": 0,
                "__v": 0
            }
        ]
    }
}
```


### 交易处理接口

此接口在后台支付界面中 我已经写好了 不需要请求这个接口 只需要请求**支付接口**即可

### 获取订单总数接口

| 请求地址             | 请求方式 | 参数类型 |
| -------------------- | -------- | -------- |
| /api/order/order_count | GET      | 无参数    |

#### 请求示例


```
http://localhost:3000/api/order/order_count
```

#### 请求参数

无参数

#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "count": 7
    }
}
```


## 活动模块

### 添加活动接口

| 请求地址                   | 请求方式 | 参数类型 |
| -------------------------- | -------- | -------- |
| /api/actively/add_actively | POST     | body     |

#### 请求示例


```js
{
    activelyNo:"",
    activelyTitle:"",
    activelyDate:""
}
```

#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
activelyNo | 活动编号 | 必选 | zpQQt4tU72zn8cD1__jsZ 
activelyTitle | 活动标题 | 必选 | zpQQt4tU72zn8cD1__jsZ 
activelyDate | 活动日期 | 必选 | 123132456465(时间戳) 格式为 2022-04-22 08:00:00 转换为时间戳 

#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "activelyNo": "003",
        "activelyTitle": "测试活动",
        "activelyDate": 1650782460000,
        "isDelete": "0",
        "_id": "626c0468f0b175a5b897273b",
        "__v": 0
    }
}
```


### 删除活动接口

| 请求地址                      | 请求方式 | 参数类型 |
| ----------------------------- | -------- | -------- |
| /api/actively/delete_actively | DELETE   | query    |

#### 请求示例


```
http://localhost:3000/api/actively/delete_actively?_id=6264f0d0567676eda9edb920
```


#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
_id | ID | 必选 | 626b6c7f7b6be0c8db787b33


#### 响应示例

```js
{
    "code": 201,
    "message": "success",
    "data": {}
}
```



### 查询所有活动接口

| 请求地址                    | 请求方式 | 参数类型 |
| --------------------------- | -------- | -------- |
| /api/actively/get_activelys | GET      | 无参数   |

#### 请求示例


```
http://localhost:3000/api/actively/get_activelys
```


#### 请求参数

无参数

#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "activelys": [
            {
                "_id": "626bbcee0b45c0dad10a710d",
                "activelyNo": "Cni9Kgax0Iwx-oCy2QhCr",
                "activelyTitle": "sad",
                "activelyDate": 1651190400000,
                "isDelete": "0",
                "__v": 0
            },
            {
                "_id": "626bbd2d0b45c0dad10a7117",
                "activelyNo": "C2bqqiR7_SHWJGhRkfrSh",
                "activelyTitle": "乌拉",
                "activelyDate": 1648771200000,
                "isDelete": "0",
                "__v": 0
            },
            {
                "_id": "626bbd3f0b45c0dad10a711c",
                "activelyNo": "OsSdR1J1hJmurnCA3KEw7",
                "activelyTitle": "999",
                "activelyDate": 1648771200000,
                "isDelete": "0",
                "__v": 0
            },
            {
                "_id": "626bbe4cccd24a85b1270f6b",
                "activelyNo": "H-0beHCajODTTJ9B-d__u",
                "activelyTitle": "测试活动",
                "activelyDate": 1648857600000,
                "isDelete": "0",
                "__v": 0
            }
        ]
    }
}
```


## 邮箱模块

### 当前用户邮件列表接口

| 请求地址                  | 请求方式 | 参数类型 |
| ------------------------- | -------- | -------- |
| /api/postman/get_postmans | GET      | query    |

#### 请求示例


```
http://localhost:3000/api/postman/get_postmans?_id=62552a7376000000ba0006e4
```


#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
_id | ID | 必选 | 626b6c7f7b6be0c8db787b33

#### 响应示例


```
{
    "code": 201,
    "message": "success",
    "data": {
        "postmans": [
            {
                "_id": "626ba0cacbd9f0f996f41e3b",
                "postmanNo": "KLrk-5a_UFSFWDMaz7NDx",
                "postmanTitle": "rt",
                "postmanContent": "oiuouioui",
                "postmanDate": 1651220682188,
                "toId": "62552a7376000000ba0006e4",
                "from": "Tom",
                "fromId": "62552a7376000000ba0006e4",
                "readSatus": 1,
                "fromRole": "student",
                "isDelete": 0,
                "__v": 0
            },
            {
                "_id": "626ba01bfabee4430925eb91",
                "postmanNo": "Gycd-BUD7juK9v4ameAB0",
                "postmanTitle": "wulaalallala",
                "postmanContent": "6666666",
                "postmanDate": 1651220507149,
                "toId": "62552a7376000000ba0006e4",
                "from": "Tom",
                "fromId": "62552a7376000000ba0006e4",
                "readSatus": 1,
                "fromRole": "student",
                "isDelete": 0,
                "__v": 0
            }
        ]
    }
}
```


### 发送邮件接口

| 请求地址                 | 请求方式 | 参数类型 |
| ------------------------ | -------- | -------- |
| /api/postman/add_postman | POST     | body     |

#### 请求示例


```
{
    postmanNo:"",
    postmanTitle:"",
    postmanContent:"",
    postmanDate:"",
    toId:"",
    toName:"",
    from:"",
    fromId:""
}
```


#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
postmanNo | 邮件编号 | 必选 | 626b6c7f7b6be0c8db787b33
postmanTitle | 邮件标题 | 必选 | 测试标题
postmanContent | 邮件内容 | 必选 | 测试内容
postmanDate | 邮件创建日期 | 必选 | 123446578(时间戳)
toId | 收件人ID | 必选 | 626b6c7f7b6be0c8db787b33 
fromRole | 发件人身份角色 | 必选 | student \| teacher （通过用户信息的 role 字段获取）
from | 发件人姓名 | 必选 | Lucson
fromId | 发件人ID | 必选 | 626b6c7f7b6be0c8db787b33

#### 响应示例


```js
{
	"code": 201,
	"message": "success",
	"data": {
		"postmanNo": "_xzPRJ_F8XPQYHFoMLjzF",
		"postmanTitle": "cs",
		"postmanContent": "qqwewqeqwe",
		"postmanDate": 1651246881952,
		"toId": "626ad3470ba635e867d51546",
		"from": "Tom",
		"fromId": "62552a7376000000ba0006e4",
		"readSatus": 0, // 是否已读 0 未读 1 已读
		"fromRole": "student",
		"isDelete": 0,
		"_id": "626c07215f477e46f40590e5",
		"__v": 0
	}
}
```


### 邮件修改已读接口

| 请求地址                           | 请求方式 | 参数类型 |
| ---------------------------------- | -------- | -------- |
| /api/postman/update_postman_status | PUT      | body     |

#### 请求示例


```js
{
    _id:"6254695153050000bc004d2c"
}
```


#### 请求参数

参数名 | 参数解释 | 是否必选 | 示例值(都是测试数据)
---|---|---|---
_id | 邮件ID | 必选 | 6254695153050000bc004d2c

#### 响应示例


```js
{
    "code": 201,
    "message": "success",
    "data": {
        "msg": "修改状态成功"
    }
}
```

