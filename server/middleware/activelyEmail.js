const nodemailer = require("nodemailer")
const config = require("../config")



const  log4j = require("log4js")
const logger = log4j.getLogger();

module.exports = async (emails,date,title) => {
         try{
            let transporter = nodemailer.createTransport({
                host: config.emailServer,
                port: 465,
                secure: true,
                auth: {
                    user: config.sendEmailUser,
                    pass: config.emailPass, // 签发的密匙
                }
            });
            return await transporter.sendMail({
                from: '教务系统活动通知 <'+config.sendEmailUser+'>', // sender address
                to: emails, // list of receivers
                subject: "院校活动提醒通知", // Subject line
                html: ` 亲爱的同学，名为 ${title} 的活动将在 ${date} 展开 请注意参加！ `,
            });
         }catch(error){
              logger.error("  发送邮件失败" + "-" + error.message)
         }
}