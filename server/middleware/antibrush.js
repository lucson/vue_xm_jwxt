// 接口防刷的中间件
const config = require("../config");

const ResponseMessage = require("../response/message");

const log4js = require("log4js");
const logger = log4js.getLogger();

module.exports = (req,res,next)=>{
        if(!req.session[req.ip+"-"+req.originalUrl]){
            req.session[req.ip+"-"+req.originalUrl] = 1;
            req.session.cookie.maxAge = config.outTime;
            next();
        }else{
            if(req.session[req.ip+"-"+req.originalUrl] >= config.reqCount){
                logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 请求过于频繁,请稍后重试")
                return res.send(new ResponseMessage(403,"接口请求过于频繁，请稍后再试...",{})) 
            }else{
                ++req.session[req.ip+"-"+req.originalUrl];
                next();
            }
        }  
} 