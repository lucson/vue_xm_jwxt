const { validationResult } = require('express-validator');
const ResponseMessage = require("../response/message");

module.exports = (validations) => {
    return async (req, res, next) => {
        await Promise.all(validations.map(validation => validation.run(req)));
        const errors = validationResult(req);
        if (errors.isEmpty()) {
            return next();
        }
        res.status(201).json(new ResponseMessage(400,errors.array(),{}));
    };
};