const express = require("express");
const router = express.Router();
const payController = require("../controller/payController")

const {
    payParamsValidator,
    payHandlerParamsValidator
} = require("../validate/payValidator")

// 接口防刷中间件
const  anibrush = require("../middleware/antibrush");   

/**
 * 登录态中间件 - 用于访问页面验证/用于需要登录的接口验证
 */
const { auth } = require("../middleware/auth");

router.get("/",[anibrush,payParamsValidator],payController.pay)

router.get("/pay_handler",[payHandlerParamsValidator],payController.handlerPay)

router.get("/return_url",payController.return_handler)

router.get("/notify_handler",payController.notify_handler)

module.exports = router;
