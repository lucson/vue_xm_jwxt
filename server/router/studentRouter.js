const express = require("express");

const router = express.Router();

const config = require("../config");

const studentController = require("../controller/studentController");

const {
  newAddStudentParamsValidator,
  getStudentInfosParamsValidator,
  deleteStudentParamsValidator,
  findStudentParamsValidator,
  addStudentParamsValidator,
  updateStudentParamsValidator
} = require("../validate/studentValidator");

const anibrush = require("../middleware/antibrush");

const { auth } = require("../middleware/auth");

const uploadFile = require("../middleware/uploadFile")

const sendEmail = require("../middleware/sendEmail")

const path = require("path");

const multer  = require('multer') 

const upload = multer({ dest: path.resolve(__dirname,'../'+config.uploadDirName) })  

/**
 * 获取学生总数
 * /api/student/count
 */
router.get("/count",  auth, studentController.StudentCount);

/**
 * 根据时间范围查询新增的学生数量
 * /api/student/new_add_student
 */
router.get("/new_add_student",[anibrush, auth, newAddStudentParamsValidator], studentController.newAddStudent);

/**
 * 获取明星学生
 * /api/student/get_excellent_student
 */
router.get("/get_excellent_student", [ auth],studentController.getExcellentStudent);

/**
 * 获取学生名单信息
 * /api/student/get_student_infos
 */
router.get("/get_student_infos",[anibrush, auth,getStudentInfosParamsValidator], studentController.getStudentInfos);

/**
 * 根据ID删除学生
 * /api/student/delete_student
 */
router.delete("/delete_student",[anibrush, auth,deleteStudentParamsValidator],studentController.deleteStudentById)

/**
 * 根据ID获取学生信息
 * /api/student/get_student
 */
router.get("/get_student",[anibrush, auth,findStudentParamsValidator],studentController.findStudentById)

/**
 * 添加学生
 * /api/student/add_student
 * 
 */
router.post("/add_student", upload.single("studentAvatar"),[anibrush, auth,addStudentParamsValidator], uploadFile("student"),studentController.addStudent,sendEmail("student") )

/**
 * 根据ID修改学生
 */
router.put("/update_student",upload.single("studentAvatar"),[anibrush, auth,updateStudentParamsValidator], uploadFile("student"),studentController.updateStudent,sendEmail("student","update"))


router.get("/student",[anibrush,auth],studentController.findStudentByStudentNo)

module.exports = router;