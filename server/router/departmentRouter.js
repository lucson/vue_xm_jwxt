const express = require("express");
const router = express.Router();

const {
    getTeacherInfosParamsValidator,
    deleteDepartmentParamsValidator,
    addTeacherParamsValidator,
    updateTeacherParamsValidator
} = require("../validate/departmentValidator")

const departmentController = require("../controller/departmentController")

const  anibrush = require("../middleware/antibrush");   

const { auth } = require("../middleware/auth");

/**
 * 获取学院总数
 * /api/department/count
 */
router.get("/count",[auth],departmentController.departmentCount)
/**
 * 获取学院列表
 */
router.get("/get_departments",[anibrush,auth,getTeacherInfosParamsValidator],departmentController.getDepartments)

/**
 * ID删除学院
 */
router.delete("/delete_department",[anibrush,auth,deleteDepartmentParamsValidator],departmentController.deleteDepartmentById)

/**
 * 添加一个学院
 */
router.post("/add_department",[anibrush,auth,addTeacherParamsValidator],departmentController.addDepartment)

/**
 * 修改学院信息
 */
router.put("/update_department",[anibrush,auth,updateTeacherParamsValidator],departmentController.updateDepartment )

router.get("/get_department",[anibrush,auth],departmentController.findDepartmentById)

module.exports = router;