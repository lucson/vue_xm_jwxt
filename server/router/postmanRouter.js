const express = require("express");
const router = express.Router();
const postmanController = require("../controller/postmanController")

const { getPoatmanParamsValidator , addPoatmanParamsValidator , updateStatusPoatmanParamsValidator} = require("../validate/postmanValidator")

// 接口防刷中间件
const  anibrush = require("../middleware/antibrush");   

/**
 * 登录态中间件 - 用于访问页面验证/用于需要登录的接口验证
 */
const { auth } = require("../middleware/auth");

router.get("/get_postmans",[anibrush,auth,getPoatmanParamsValidator],postmanController.getPostmans)

router.post("/add_postman",[anibrush,auth,addPoatmanParamsValidator],postmanController.addPostman)

router.put("/update_postman_status",[anibrush,auth,updateStatusPoatmanParamsValidator],postmanController.updatePostmanStatus)

module.exports = router;