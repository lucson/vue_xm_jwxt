const express = require("express");
const router = express.Router();
const activelyController = require("../controller/activelyController")

// 接口防刷中间件
const  anibrush = require("../middleware/antibrush");   

const { addActivelyParamsValidator , deleteActivelyParamsValidator  } = require("../validate/activelyValidator")

/**
 * 登录态中间件 - 用于访问页面验证/用于需要登录的接口验证
 */
const { auth } = require("../middleware/auth");

router.get("/get_news_actively",[auth],activelyController.getNewsActively)

router.post("/add_actively",[anibrush,auth,addActivelyParamsValidator],activelyController.addNewActively)

router.get("/get_activelys",[anibrush,auth],activelyController.getNewsActively)

router.delete("/delete_actively",[anibrush,auth,deleteActivelyParamsValidator],activelyController.deleteActivelyById)

module.exports = router;
 
 
