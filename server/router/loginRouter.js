const express = require("express");
const router = express.Router();

const { 
    loginCore , 
    loginAuth , 
    logoutCore 
} = require("../controller/loginController")

const { 
    loginParamsValidator , 
    loginUserExistsValidator , 
    loginAuthParamsValidator ,
    logoutParamsValidator
} = require("../validate/loginValidator")


// 接口防刷中间件
const  anibrush = require("../middleware/antibrush");   

/**
 * 登录态中间件 - 用于访问页面验证/用于需要登录的接口验证
 */
const { auth } = require("../middleware/auth");

/**
 * 登录接口
 * /api/login
 */
router.post("/login",[anibrush,loginParamsValidator,loginUserExistsValidator],loginCore)

/**
 * 认证接口
 * /api/auth
 * 用于访问页面时判断登录态是否过期
 */
router.post("/auth",[anibrush,loginAuthParamsValidator,auth],loginAuth)


/**
 * 登出接口
 * /api/logout
 * 用于用户退出
 */
router.get("/logout",[anibrush,logoutParamsValidator,auth],logoutCore)


module.exports = router;