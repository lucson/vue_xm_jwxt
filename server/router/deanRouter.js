const express = require("express");
const router = express.Router();
const deanController = require("../controller/deanController")

const  anibrush = require("../middleware/antibrush");  

const { auth } = require("../middleware/auth");

/**
 * 获取到院长列表
 */
router.get("/get_deans",[anibrush,auth],deanController.getDeans);

module.exports = router;