const express = require("express");
const router = express.Router();
const teacherController = require("../controller/teacherController")

const {
    getTeacherInfosParamsValidator,
    findTeacherParamsValidator,
    addTeacherParamsValidator,
    updateTeacherParamsValidator,
    deleteTeacherParamsValidator
} = require("../validate/teacherValidator")

const antibrush = require("../middleware/antibrush")

const {  auth } = require("../middleware/auth")

const uploadFile = require("../middleware/uploadFile")

const sendEmail = require("../middleware/sendEmail")

const path = require("path");

const multer  = require('multer') 

const config = require("../config")

const upload = multer({ dest: path.resolve(__dirname,'../'+config.uploadDirName) })  

router.get("/get_teacher_infos",[antibrush, auth,  getTeacherInfosParamsValidator],teacherController.getTeacherInfos)

router.get("/get_teacher",[antibrush, auth, findTeacherParamsValidator],teacherController.findTeacherById);

router.post("/add_teacher",upload.single("teacherAvatar"),[antibrush, auth, addTeacherParamsValidator],uploadFile("teacher"),teacherController.addTeacher,sendEmail("teacher"))

router.put("/update_teacher",upload.single("teacherAvatar"),[antibrush,auth, updateTeacherParamsValidator], uploadFile("teacher"),teacherController.updateTeacher,sendEmail("teacher","update"))

router.delete("/delete_teacher",[antibrush,auth,deleteTeacherParamsValidator],teacherController.deleteTeacherById)

router.get("/get_teacher_count",[antibrush,auth,],teacherController.getTeacherCount)

module.exports = router;