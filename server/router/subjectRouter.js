const express = require("express");
const router = express.Router();
const subjectController = require("../controller/subjectController")

const {
    getSubjectInfosParamsValidator,
    deleteSubjectParamsValidator,
    addSubjectParamsValidator,
    updateTeacherParamsValidator
} = require("../validate/subjectValidator")

const antibrush = require("../middleware/antibrush")

const {  auth } = require("../middleware/auth")

router.get("/get_teacher_infos",[antibrush,getSubjectInfosParamsValidator],subjectController.getSubjectInfos)

router.post("/add_subject",[antibrush,auth,addSubjectParamsValidator],subjectController.addSubject)

router.put("/update_subject",[updateTeacherParamsValidator],subjectController.updateSubject)

router.delete("/delete_subject",[antibrush,auth,deleteSubjectParamsValidator],subjectController.deleteSubjectById)

router.get("/get_subject_count",antibrush,auth,subjectController.getSubjectCount)

router.get("/get_subject",antibrush,auth,subjectController.getSubjectById)

module.exports = router;