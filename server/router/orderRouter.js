const express = require("express");
const router = express.Router();
const orderController = require("../controller/orderController")
const { 
    tradePriceParamsValidator,
    getOrderInfosParamsValidator,
    deleteOrderParamsValidator,
    findOrderByOrderNoParamsValidator,
    addOrderParamsValidator
} = require("../validate/orderValidator")

// 接口防刷中间件
const  anibrush = require("../middleware/antibrush");   

/**
 * 登录态中间件 - 用于访问页面验证/用于需要登录的接口验证
 */
const { auth } = require("../middleware/auth");

/**
 * 获取订单总交易额
 * /api/order/total_price
 */
router.get("/total_price",[auth],orderController.OrderTotalPrice)

/**
 * 根据时间戳范围选择查询交易额
 */
router.get("/cycle_trade_price",[anibrush,auth,tradePriceParamsValidator],orderController.CycleTradePrice)

/**
 * 获取订单信息
 */
router.get("/get_orders",[anibrush,auth,getOrderInfosParamsValidator],orderController.getOrderInfos)

/**
 * 删除订单
 */
router.delete("/delete_order",[anibrush,auth,deleteOrderParamsValidator],orderController.deleteOrderById)

/**
 * 根据订单编号查询一个订单
 */
router.get("/get_order",[anibrush,auth,findOrderByOrderNoParamsValidator],orderController.findOrderByOrderNo)

/**
 * 创建一个订单
 */
router.post("/add_order",[anibrush,auth,addOrderParamsValidator],orderController.addOrder)


router.get("/order_count",[anibrush,auth],orderController.findOrderCount)

module.exports = router;