module.exports = {
    protocal:"http",
    hostname:"192.168.0.102", // 此处建议写局域网IP
    port:3000,
    // 允许的域 必须要改 查看自己的局域网IP 地址写前端地址
    origin:"http://192.168.0.102:8080",
    // 数据库链接地址
    dbUrl:"mongodb://admin:123456@192.168.0.102:27001,192.168.0.102:27002/jwxt?replicaSet=rs0",
    // JWT 需要的配置 START
    secret:"9f321e43-989c-4d0b-8c37-be61bf292e11",
    header:{
        typ: 'JWT',
        alg: 'HS256'
    },
    crypto:"sha256",
    expire:60 * 1000 * 60 * 24, // 一天
    // expire:20 * 1000,
    // JWT 需要的配置 END
    // 防刷需要的配置 START
    host:"0.0.0.0", // 不要修改
    outTime:20 * 1000,
    reqCount:40,
    // 防刷需要的配置 END
    uploadDirName:"uploads",
    uploadTypes:["image/png","image/jpeg","image/jpg"],
    uploadSize:0.5 * 1024 * 1024, // 500 kb
    sendEmailUser:"15236148260@163.com", // 发件人邮箱 哪个账号生成的密匙就用那个账号
    emailPass:"这里填写你的邮箱密匙 需要去邮件服务里开启 并生成获取密匙 否则不会发邮件",
    emailServer:"smtp.163.com", // 邮件服务器 可以用 163 或 qq 这里用 163
    // 订单过期时间配置
    // orderExpireTime:60 * 1000 * 60 * 24,
    orderExpireTime:20*1000,
    // 支付频道地址
    sockets:{
        pay:"/pay"
    },
}