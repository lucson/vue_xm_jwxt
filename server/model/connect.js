const mongoose = require("mongoose");
const config = require("../config");
mongoose.connect(config.dbUrl,{
    useNewUrlParser:true,
    useUnifiedTopology:true
});
const db = mongoose.connection;
db.on("error",function (err) {
    console.log("connect error"+err)
});
db.on("open",function f() {
    console.log("connect success")
});
