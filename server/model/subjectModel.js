const mongoose = require("mongoose")

const subject = mongoose.Schema({
    subjectNo:{
        type:String,
        required:true,
        select:true
    },
    subjectName:{
        type:String,
        required:true,
        select:true
    },
    // gradeIds:{
    //     type:String,
    //     required:true,
    //     select:true
    // },
    // departmentIds:{
    //     type:String,
    //     required:true,
    //     select:true
    // },
    isDelete:{
        type:Number,
        default:0,
        select:true
    }
})

module.exports = mongoose.model("subject",subject,"subjects")