const mongoose = require("mongoose")

const Grade = new mongoose.Schema({
    gradeNo:{
        type:String,
        required:true,
        select:true
    },
    gradeName:{
        type:String,
        required:true,
        select:true
    },
    isDelete:{
        type:Number,
        default:0,
        select:true
    }
})

module.exports = mongoose.model("grade",Grade,"grades")