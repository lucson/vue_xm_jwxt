const mongoose  = require("mongoose")

const Actively = new mongoose.Schema({
    activelyNo:{
        type:String,
        required:true,
        select:true
    },
    activelyTitle:{
        type:String,
        required:true,
        select:true
    },
    activelyDate:{
        type:Number,
        required:true,
        select:true
    },
    isDelete:{
        type:String,
        default:0,
        select:true
    }
})

module.exports = mongoose.model("actively",Actively,"activelys")