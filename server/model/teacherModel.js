
const mongoose =  require("mongoose");

const Teacher = mongoose.Schema({
    teacherNo:{
        type:String,
        required:true,
        select:true
    },
    teacherName:{
        type:String,
        required:true,
        select:true
    },
    teacherPhone:{
        type:String,
        required:true,
        select:true
    },
    teacherEmail:{
        type:String,
        required:true,
        select:true
    },
    teacherSex:{
        type:Number,
        default:1, // 1 男 2 女
        select:true
    },
    teacherBirthday:{
        type:Number,  // 存储的是时间戳
        required:true,
        select:true
    },
    teacherSubjectId:{
        type:String,
        default:"",
        select:true
    },
    teacherInSchoolDate:{
        type:Number,
        select:true
    },
    teacherAvatar:{
        type:String,
        default:"http://q4.qlogo.cn/headimg_dl?dst_uin=2857734156&spec=100",
        select:true
    },
    teacherDepartment:{
        type:Number,
        select:true
    },
    teacherNowAddress:{
        type:String,
        default:"未填写",
        select:true
    },
    teacherHomeAddress:{
        type:String,
        default:"未填写",
        select:true
    },
    teacherSkill:{
        type:String,
        default:"",
        select:true
    },
    teacherRecommend:{
        type:String,
        default:"未填写",
        select:true
    },
    teacherSalary:{
        type:Number,
        default:0,
        select:true
    },
    teacherExperience:{
        type:Number,
        default:1,  // 1 (代表 1 - 3 年) 2 (代表 3 - 5 年) 3 (代表 5 年以上) 这里可以在创建一个集合 我就不创建了
        select:true
    },
    position:{
        type:Number,
        default:1,  // 1 (代表普通教师) 2 (代表院长)  这里可以在创建一个集合 我就不创建了
        select:true
    },
    loginStatus:{
        type:Number,
        default:0,  // 0 代表未登录 1 代表登录 - 拒绝多人同时登录一个账号
        select:true
    },
    gradeIds:{
        type:String,
        default:"",
        select:true
    },
    lastLoginDate:{
        type:Number,
        select:true,
        default:0
    },
    dollar:{
        type:Number,
        default:1000,
        select:true
    },
    isDelete:{
        type:Number,
        default:0, // 0 代表未删除  1 代表删除
        select:true
    },
    permission:{
        type:Number,
        default:2, // 这个是写死的 创建的时候是什么就是什么 至于升职这个功能没有写  1 (代表学生权限) 2 (代表老师权限) 3 (代表院长权限) 4 (代表校长权限)
        select:true    
    },
    role:{
        type:String,
        default:"teacher",
        select:true
    }
})

module.exports =  mongoose.model("teacher",Teacher,"teachers")