const mongoose = require("mongoose");

const Department  =  new mongoose.Schema({
    departmentNo:{
        type:String,
        required:true,
        select:true
    },
    departmentName:{
        type:String,
        required:true,
        select:true
    },
    departmentLeader:{
        type:String,
        required:true,
        select:true
    },
    departmentCreateDate:{
        type:Number,
        required:true,
        select:true    
    },
    studentNumber:{
        type:Number,
        required:true
    },
    isDelete:{
        type:Number,
        default:0,
        select:true
    }
})

module.exports = mongoose.model("department",Department,"departments")