const mongoose = require("mongoose")

const postman = mongoose.Schema({
    postmanNo:{
        type:String,
        required:true,
        select:true
    },
    postmanTitle:{
        type:String,
        required:true,
        select:true
    },
    postmanContent:{
        type:String,
        required:true,
        select:true
    },
    postmanDate:{
        type:Number,
        required:true,
        select:true
    },
    toId:{
        type:String,
        required:true,
        select:true
    },
    from:{
        type:String,
        required:true,
        select:true
    },
    fromId:{
        type:String,
        required:true,
        select:true
    },
    readSatus:{
        type:Number,
        default:0,  // 0 代表未读   1代表已读
        select:true
    },
    fromRole:{
        type:String,
        required:true,
        select:true
    },
    isDelete:{
        type:Number,
        default:0,
        select:true
    }
})


module.exports = mongoose.model("postman",postman,"postmans")