const mongoose = require("mongoose");

const Dean =  new mongoose.Schema({
    deanNo:{
        type:String,
        required:true,
        select:true
    },
    deanName:{
        type:String,
        required:true,
        select:true
    },
    permission:{
        type:Number,
        default:3,
        select:true
    },
    role:{
        type:String,
        default:"dean",
        select:true 
    },
    isDelete:{
        type:Number,
        default:0,
        select:true
    },
    loginStatus:{
        type:Number,
        default:0,
        select:true
    }
})

module.exports =  mongoose.model("dean",Dean,"deans")