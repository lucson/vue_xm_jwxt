const mongoose = require("mongoose");

const Admin = new mongoose.Schema({
    name:{
        type:String,
        select:true,
        required:true
    },
    pwd:{
        type:String,
        select:false,
        required:true 
    },
    role:{
        type:String,
        select:true,
        default:"admin"
    },
    permission:{
        type:Number,
        select:true,
        default:4
    },
    loginStatus:{
        type:Number,
        select:true,
        default:0
    }
})

module.exports =  mongoose.model("admin",Admin,"admins")