
const {
    departmentCount,
    getDepartments,
    deleteDepartmentById,
    addDepartment,
    updateDepartment,
    findDepartmentById
} = require("../persistent_api/departmentApi");

const ResponseMessage = require("../response/message")

const log4js = require("log4js");
const logger = log4js.getLogger();

exports.findDepartmentById = async (req,res) => {
   try{
      const { id } = req.query;
      const department = await findDepartmentById(id);
      if(department){
         res.send(new ResponseMessage(201,"success",department))
      }else{
         res.send(new ResponseMessage(400,"该学院不存在",{}))
      }
   }catch(error){
      logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 获取学院失败")
      res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
   }
}

exports.departmentCount = async (req,res) => {
    try{
        const count =  await departmentCount();
        res.send(new ResponseMessage(201,"success",{count}));
    }catch(error){
        logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 获取学院总数失败")
        res.send(new ResponseMessage(500,"网络错误",{}));
    }
}

exports.getDepartments = async (req,res) => {
    try{
        const { searchType,searchContent,pageIndex,pageSize } = req.query;
        console.log(searchType,searchContent,pageIndex,pageSize);
        const departments = await getDepartments(searchContent || false,pageIndex || 1,pageSize || 12);
        if(!departments || departments.length === 0 ){
           res.send(new ResponseMessage(201,"未找到学院列表信息",{departments}));
        }else{
           res.send(new ResponseMessage(201,"success",{departments}));
        }
     }catch(error){
        logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 获取学院列表失败")
        res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
     }
}

exports.deleteDepartmentById = async (req,res) => {
    try{
       const { id } = req.query;
       const { modifiedCount } = await deleteDepartmentById(id);
       if(modifiedCount > 0){
          res.send(new ResponseMessage(201,"success",{}))
       }else{
          res.send(new ResponseMessage(400,"删除失败,该学院已删除或该学院不存在",{}))
       }
    }catch(error){
       logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 删除学院失败")
       res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
    }
 }

  /**
 * 添加一个学院
 * @param {Request} req 
 * @param {Response} res 
 */
exports.addDepartment = async (req,res,next) => {
   try{ 
      const newDepartment = req.body;
      const department = await addDepartment(newDepartment);
      res.send(new ResponseMessage(201,"success",department)) && next()
   }catch(error){
      logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 添加学院失败")
      res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
   }
}

/**
 * 修改学院信息
 * @param {Request} req 
 * @param {Response} res 
 * @param {Function} next 
 */
 exports.updateDepartment = async (req,res) => {
   try{ 
      const newDepartment = req.body;
      const department = await updateDepartment(newDepartment);
      if(department.modifiedCount && department.modifiedCount !== 0){
         return res.send(new ResponseMessage(201,"success",department))
      }else{
         return res.send(new ResponseMessage(201,"要修改的数据和已有的数据相同",department)) 
      }
   }catch(error){
      console.log(error);
      logger.error(req.ip + " - " + req.method + " - " + req.originalUrl + " - 修改学院失败")
      res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
   }
}
