const { getNewsActively,addNewActively , getActivelys , deleteActivelyById} = require("../persistent_api/activelyApi")

const { getPostmans,addPostman,updatePostmanStatus } = require("../persistent_api/postmanApi")

const log4js = require("log4js");
const logger = log4js.getLogger();

const ResponseMessage = require("../response/message")

exports.getPostmans = async (req,res) => {
    try{
        const { _id } = req.query;  
        const postmans = await getPostmans(_id);
        if(postmans.length === 0){
            res.send(new ResponseMessage(201,"暂无邮件信息",{}));
        }else{
            res.send(new ResponseMessage(201,"success",{postmans}));
        }
    }catch(error){
        logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 获取邮件列表失败")
        res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
    }
}



 exports.addPostman = async (req,res,next) => {
    try{ 
       const newPostman = req.body;
       const postman = await addPostman(newPostman);
       res.send(new ResponseMessage(201,"success",postman))
    }catch(error){
       logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 添加邮件失败")
       res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
    }
 }


exports.updatePostmanStatus = async (req,res,next) => {
    try{ 
       const { _id } = req.body;
       const {modifiedCount} = await updatePostmanStatus(_id);
       if(modifiedCount !== 0){
          return res.send(new ResponseMessage(201,"success",{msg:"修改状态成功"}))
       }else{
          return res.send(new ResponseMessage(201,"要修改的数据和已有的数据相同",{})) 
       }
    }catch(error){
       logger.error(req.ip + " - " + req.method + " - " + req.originalUrl + " - 修改学生失败")
       res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
    }
 }