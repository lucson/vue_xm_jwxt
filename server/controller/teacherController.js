
const {
    getTeacherInfos,
    findTeacherById,
    addTeacher,
    updateTeacher,
    deleteTeacherById,
    getTeacherCount
} = require("../persistent_api/teacherApi")

const log4js = require("log4js");
const logger = log4js.getLogger();

const ResponseMessage = require("../response/message");

/**
 * 获取老师名单信息
 * @param {Request} req 
 * @param {Response} res 
 */
 exports.getTeacherInfos = async (req,res) => {
    try{
       const { searchType,searchContent,pageIndex,pageSize } = req.query;
       const teacher = await getTeacherInfos(searchType,searchContent,pageIndex,pageSize);
       if(!teacher || teacher.length === 0 ){
          res.send(new ResponseMessage(201,"未找到教师信息",{teacher}));
       }else{
          res.send(new ResponseMessage(201,"success",{teacher}));
       }
    }catch(error){
       logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 获取教师信息失败")
       res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
    }
 }

 /**
  * 根据ID查找一个教师
  * @param {Request} req 
  * @param {Response} res 
  */
 exports.findTeacherById = async (req,res) => {
   try{
      const { id } = req.query;
      const teacher = await findTeacherById(id);
      if(!teacher){
         res.send(new ResponseMessage(201,"未找到教师信息",{}))
      }else{
         res.send(new ResponseMessage(201,"success",{teacher}))
      }
   }catch(error){
      logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 获取教师信息失败")
      res.send(new ResponseMessage(500,"没有这个教师的信息",{}));
   }
 }

 /**
 * 添加一个教师
 * @param {Request} req 
 * @param {Response} res 
 */
exports.addTeacher = async (req,res,next) => {
   try{ 
      const newTeacher = req.body;
      const teacher = await addTeacher(newTeacher);
      res.send(new ResponseMessage(201,"success",teacher)) && next()
   }catch(error){
      logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 添加教师失败")
      res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
   }
}

/**
 * 修改教师信息
 * @param {Request} req 
 * @param {Response} res 
 * @param {Function} next 
 */
 exports.updateTeacher = async (req,res,next) => {
   try{ 
      const newTeacher = req.body;
      console.log(newTeacher);
      const teacher = await updateTeacher(newTeacher);
      if(teacher.modifiedCount !== 0){
         return res.send(new ResponseMessage(201,"success",teacher)) &&  next()
      }else{
         return res.send(new ResponseMessage(201,"要修改的数据和已有的数据相同",teacher)) 
      }
   }catch(error){
      logger.error(req.ip + " - " + req.method + " - " + req.originalUrl + " - 修改教师失败")
      res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
   }
}

/**
 * 根据ID删除一个教师
 * @param {Request} req 
 * @param {Response} res 
 */
exports.deleteTeacherById = async (req,res) => {
   try{
      const { id } = req.query;
      const { modifiedCount } = await deleteTeacherById(id);
      if(modifiedCount > 0){
         res.send(new ResponseMessage(201,"success",{}))
      }else{
         res.send(new ResponseMessage(400,"删除失败,该教师已删除或该用户不存在",{}))
      }
   }catch(error){
      logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 删除教师失败")
      res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
   }
}

/**
 * 获取教师总数
 * @param {Request} req 
 * @param {Response} res 
 */
exports.getTeacherCount = async (req,res) => {
   try{
      res.send(new ResponseMessage(400,"success",{count:await getTeacherCount()}))
   }catch(error){
      logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 删除教师失败")
      res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
   }
}