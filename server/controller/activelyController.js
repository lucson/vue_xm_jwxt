const { getNewsActively,addNewActively , getActivelys , deleteActivelyById} = require("../persistent_api/activelyApi")

const { getStudentsEmail } = require("../persistent_api/studentApi")

const log4js = require("log4js");
const logger = log4js.getLogger();

const ResponseMessage = require("../response/message")

const schedule = require('node-schedule');

const activelyEmail = require("../middleware/activelyEmail")  
/**
 * 获取到最新的学生活动
 * @param {Request} req 
 * @param {Response} res 
 */
exports.getNewsActively = async (req,res) => {
    try{
        const newsActively = await getNewsActively();
        if(newsActively.length === 0){
            res.send(new ResponseMessage(201,"暂无活动信息",{}));
        }else{
            res.send(new ResponseMessage(201,"success",{newsActively}));
        }
    }catch(error){
        logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 获取最新活动失败")
        res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
    }
}

/**
 * 获取到所有活动信息
 * @param {Request} req 
 * @param {Response} res 
 */
exports.getNewsActively = async (req,res) => {
    try{
        const activelys = await getActivelys();
        if(activelys.length === 0){
            res.send(new ResponseMessage(201,"暂无活动信息",{}));
        }else{
            res.send(new ResponseMessage(201,"success",{activelys}));
        }
    }catch(error){
        logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 获取活动失败")
        res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
    }
}

exports.deleteActivelyById = async (req,res) => {
    try{
       const { _id } = req.query;
       const { modifiedCount } = await deleteActivelyById(_id);
       if(modifiedCount > 0){
          res.send(new ResponseMessage(201,"success",{}))
       }else{
          res.send(new ResponseMessage(400,"删除失败,该活动已删除或该活动不存在",{}))
       }
    }catch(error){
       logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 删除活动失败")
       res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
    }
 }

/**
 * 添加一个新活动
 * @param {Request} req 
 * @param {Response} res 
 */
exports.addNewActively = async (req,res) => {
    let j = null;;
    try{
        const actively = req.body;
        const newsActively = await addNewActively(actively);
        const students = await getStudentsEmail();
        const emails = students.map((cur) => {
            return cur.studentEmail;
        })
        // 启动定时任务
        // 这里只是做了添加活动的定时任务 其实完整的应该设置一个保存定时任务的集合 甚至于做一个持久化 因为可能服务器会重启
        // 再删除的时候找到对应的定时任务取消掉
        // 那么这里就没做了 给注释掉能更直观的看到发邮件的效果
        // j = schedule.scheduleJob(new Date(newsActively.activelyDate), function(){
            console.log("发送活动通知邮件")
            logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 发送活动通知邮件")
            activelyEmail(emails,new Date(newsActively.activelyDate).toLocaleDateString(),newsActively.activelyTitle);
        // });
        res.send(new ResponseMessage(201,"success",newsActively))
    }catch(error){
        // j.cancel();
        logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 添加活动失败")
        res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
    }
}
