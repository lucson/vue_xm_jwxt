const {
    getDeans
} = require("../persistent_api/deanApi")


const ResponseMessage = require("../response/message")

const log4js = require("log4js");
const logger = log4js.getLogger();

/**
 * 获取到所有院长
 * @param {Request} req 
 * @param {Response} res 
 */
exports.getDeans = async (req,res) => {
    try{
        const deans = await getDeans();
        console.log(deans);
        if(!deans || deans.length === 0 ){
           res.send(new ResponseMessage(201,"未找到学院长相关信息",{deans}));
        }else{
           res.send(new ResponseMessage(201,"success",{deans}));
        }
     }catch(error){
        logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 获取院长列表失败")
        res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
     }
}