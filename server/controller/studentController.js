const {
    studentCount,
    newAddStudent,
    getExcellentStudent,
    getStudentInfos,
    deleteStudentById,
    findStudentById,
    addStudent,
    updateStudent,
    findStudentByStudentNo
} = require("../persistent_api/studentApi")

const log4js = require("log4js");
const logger = log4js.getLogger();

const ResponseMessage = require("../response/message");

/**
 * 总学生数
 * @param {Request} req 
 * @param {Response} res 
 */
exports.StudentCount = async (req,res) => {
     try{
        const count = await studentCount();
        res.send(new ResponseMessage(201,"success",{count}));
     }catch(error){
        logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 获取学生总数失败")
        res.send(new ResponseMessage(500,"网络错误",{}));
     }
}
/**
 * 根据时间范围查询新增的学生数量
 * @param {Request} req 
 * @param {Response} res 
 */
exports.newAddStudent = async (req,res) => {
   try{
      const { cycleType } = req.query;
      const count = await newAddStudent(cycleType);
      if(!count || count.length === 0 ){
         res.send(new ResponseMessage(201,"未找到想关时间范围的的新增学生信息",{count}));
      }else{
         res.send(new ResponseMessage(201,"success",{count}));
      }
   }catch(error){
      logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 获取学生总数失败")
      res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
   }
}

/**
 * 获取明星学生
 * @param {Request} req 
 * @param {Response} res 
 */
exports.getExcellentStudent = async (req,res) => {
   try{
      const count = await getExcellentStudent();
      if(!count || count.length === 0 ){
         res.send(new ResponseMessage(201,"未找到明星学生信息",{count}));
      }else{
         res.send(new ResponseMessage(201,"success",{count}));
      }
   }catch(error){
      logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 获取明星学生信息失败")
      res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
   }
}

/**
 * 获取学生名单信息
 * @param {Request} req 
 * @param {Response} res 
 */
exports.getStudentInfos = async (req,res) => {
   try{
      const { searchType,searchContent,pageIndex,pageSize } = req.query;
      const studens = await getStudentInfos(searchType,searchContent,pageIndex,pageSize);
      if(!studens || studens.length === 0 ){
         res.send(new ResponseMessage(201,"未找到学生信息",{studens}));
      }else{
         res.send(new ResponseMessage(201,"success",{studens}));
      }
   }catch(error){
      logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 获取学生信息失败")
      res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
   }
}

/**
 * 根据ID删除学生
 * @param {String} id 
 */
exports.deleteStudentById = async (req,res) => {
   try{
      const { id } = req.query;
      const { modifiedCount } = await deleteStudentById(id);
      if(modifiedCount > 0){
         res.send(new ResponseMessage(201,"success",{}))
      }else{
         res.send(new ResponseMessage(400,"删除失败,该学生已删除或该用户不存在",{}))
      }
   }catch(error){
      logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 删除学生失败")
      res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
   }
}

/**..
 * 根据ID查找一个学生
 * @param {Request} req 
 * @param {Response} res 
 */
exports.findStudentById = async (req,res) => {
   try{
      const { id } = req.query;
      const student = await findStudentById(id);
      if(!student){
         res.send(new ResponseMessage(201,"未找到学生信息",{}))
      }else{
         res.send(new ResponseMessage(400,"success",{student}))
      }
   }catch(error){
      console.log(error);
      logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 获取学生信息失败")
      res.send(new ResponseMessage(500,"没有这个学生的信息",{}));
   }
}

exports.findStudentByStudentNo = async (req,res) => {
   try{
      const { studentNo } = req.query;
      const student = await findStudentByStudentNo(studentNo);
      if(!student){
         res.send(new ResponseMessage(201,"未找到学生信息",{}))
      }else{
         res.send(new ResponseMessage(400,"success",{student}))
      }
   }catch(error){
      console.log(error);
      logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 获取学生信息失败")
      res.send(new ResponseMessage(500,"没有这个学生的信息",{}));
   }
}



/**
 * 添加一个学生
 * @param {Request} req 
 * @param {Response} res 
 */
exports.addStudent = async (req,res,next) => {
   try{ 
      const newStudent = req.body;
      newStudent.studentTotalScore = parseInt(Math.random()*1500+1)
      const student = await addStudent(newStudent);
      res.send(new ResponseMessage(201,"success",student)) && next()
   }catch(error){
      logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 添加学生失败")
      res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
   }
}

/**
 * 修改学生信息
 * @param {Request} req 
 * @param {Response} res 
 * @param {Function} next 
 */
exports.updateStudent = async (req,res,next) => {
   try{ 
      const newStudent = req.body;
      const student = await updateStudent(newStudent);
      if(student.modifiedCount !== 0){
         return res.send(new ResponseMessage(201,"success",student)) &&  next()
      }else{
         return res.send(new ResponseMessage(201,"要修改的数据和已有的数据相同",student)) 
      }
   }catch(error){
      logger.error(req.ip + " - " + req.method + " - " + req.originalUrl + " - 修改学生失败")
      res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
   }
}