const {
    updateOrderStatus,
    orderTotalPrice,
    cycleTradePrice,
    getOrders,
    deleteOrderById,
    findOrderByOrderNo,
    addOrder,
    getOrderCount
} = require("../persistent_api/orderApi")

const log4js = require("log4js");
const logger = log4js.getLogger();

const ResponseMessage = require("../response/message");
const config = require("../config");


exports.findOrderByOrderNo = async (req,res) => {
   try{
      const {orderNo} = req.query;
      const order = await findOrderByOrderNo(orderNo);
      res.send(new ResponseMessage(201,"success",{order}));
   }catch(error){
      logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 获取订单信息失败")
      res.send(new ResponseMessage(500,"网络错误",{}));
   }
}

exports.findOrderCount = async (req,res) => {
   try{
      const count = await getOrderCount();
      res.send(new ResponseMessage(201,"success",{count}));
   }catch(error){
      logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 获取订单信息失败")
      res.send(new ResponseMessage(500,"网络错误",{}));
   }
}

exports.OrderTotalPrice = async (req,res) => {
     try{
        const totalPrice = await orderTotalPrice();
        res.send(new ResponseMessage(201,"success",{totalPrice}));
     }catch(error){
        logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 获取总交易额失败")
        res.send(new ResponseMessage,500,"网络错误",{});
     }
}

/**
 * 根据时间范围查询到具体的交易额
 * @param {Request} req 
 * @param {Response} res 
 */
exports.CycleTradePrice = async (req,res) => {
   try{
      const { cycleType } = req.query;
      const tradePrice = await cycleTradePrice(cycleType);
      if(!tradePrice || tradePrice.length === 0 ){
         res.send(new ResponseMessage(201,"未找到想关时间范围的的交易额信息",{tradePrice}));
      }else{
         res.send(new ResponseMessage(201,"success",{tradePrice}));
      }
   }catch(error){
        logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 根据时间戳范围获取交易额失败")
        res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
   }
}

/**
 * 获取订单信息
 * @param {Request} req 
 * @param {Response} res 
 */
 exports.getOrderInfos = async (req,res) => {
   try{
      const {searchContent,pageIndex,pageSize } = req.query;
      const order = await getOrders(searchContent,pageIndex,pageSize);
      if(!order || order.length === 0 ){
         res.send(new ResponseMessage(201,"未找到订单信息",{order}));
      }else{
         res.send(new ResponseMessage(201,"success",{order}));
      }
   }catch(error){
      logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 获取订单信息失败")
      res.send(new ResponseMessage(5000,"网络错误",{msg:error.message}));
   }
}

/**
 * 删除订单
 * @param {Request} req 
 * @param {Response} res 
 */
exports.deleteOrderById = async (req,res) => {
   try{
      const { id } = req.query;
      const { modifiedCount } = await deleteOrderById(id);
      if(modifiedCount > 0){
         res.send(new ResponseMessage(201,"success",{}))
      }else{
         res.send(new ResponseMessage(400,"删除失败,该订单已删除或该订单不存在",{}))
      }
   }catch(error){
      logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 删除订单失败")
      res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
   }
}

/**
 * 添加一个订单
 * @param {Request} req 
 * @param {Response} res 
 */
  exports.addOrder = async (req,res,next) => {
   try{ 
      const newOrder = req.body;
      newOrder.tradeCreateDate = Date.now(); 
      newOrder.tradeOutDate = Date.now() + config.orderExpireTime;
      const order = await addOrder(newOrder);
      res.send(new ResponseMessage(201,"success",{orderNo:order.orderNo,id:order._id})) && next()
   }catch(error){
      logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 添加订单失败")
      res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
   }
}

/**
 * 修改订单的状态
 * @param {String} orderNo
 * @param {Number} status {0|1|2} 
 */
exports.updateOrderStatus = async (orderNo,status) => {
   try{
      const { modifiedCount } = await updateOrderStatus(orderNo,status);
      if(modifiedCount > 0){
         res.send(new ResponseMessage(201,"修改订单状态成功",{}))
      }else{
         res.send(new ResponseMessage(201,"修改订单状态失败,该订单已删除或该订单不存在",{}))
      }
   }catch(error){
      logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 修改订单状态订单失败")
      res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
   }
}

