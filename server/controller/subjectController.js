const {
    getSubjectInfos,
    deleteSubjectById,
    addSubject,
    updateSubject,
    getSubjectCount,
    getSubjectById
} = require("../persistent_api/subjectApi")

const ResponseMessage = require("../response/message");

const log4js = require("log4js");
const logger = log4js.getLogger();

/**
 * 获取科目列表信息
 * @param {Request} req 
 * @param {Response} res 
 */
 exports.getSubjectInfos = async (req,res) => {
    try{
       const { searchContent,pageIndex,pageSize } = req.query;
       const subjects = await getSubjectInfos(searchContent,pageIndex,pageSize);
       if(!subjects || subjects.length === 0 ){
          res.send(new ResponseMessage(201,"未找到科目信息",{subjects}));
       }else{
          res.send(new ResponseMessage(201,"success",{subjects}));
       }
    }catch(error){
       logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 获取科目信息失败")
       res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
    }
 }

 

 exports.getSubjectCount = async (req,res) => {
   try{
      const count = await getSubjectCount();
      res.send(new ResponseMessage(201,"success",{count}));
   }catch(error){
      logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 获取科目信息失败")
      res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
   }
}

exports.getSubjectById = async (req,res) => {
   try{
      const { id } = req.query;
      const subject = await getSubjectById(id);
      if(subject){
         res.send(new ResponseMessage(201,"success",subject));
      }else{
         res.send(new ResponseMessage(201,"未找到科目信息,请确认ID正确",{}));
      }
   }catch(error){
      logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 获取科目信息失败")
      res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
   }
}
 /**
 * 添加一个科目
 * @param {Request} req 
 * @param {Response} res 
 */
  exports.addSubject = async (req,res,next) => {
   try{ 
      const newSubject = req.body;
      const subject = await addSubject(newSubject);
      res.send(new ResponseMessage(201,"success",subject)) && next()
   }catch(error){
      logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 添加科目失败")
      res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
   }
}

/**
 * 修改学院信息
 * @param {Request} req 
 * @param {Response} res 
 * @param {Function} next 
 */
 exports.updateSubject = async (req,res) => {
   try{ 
      const newSubject = req.body;
      const subject = await updateSubject(newSubject);
      if(subject.modifiedCount && subject.modifiedCount !== 0){
         return res.send(new ResponseMessage(201,"success",subject))
      }else{
         return res.send(new ResponseMessage(201,"要修改的数据和已有的数据相同",subject)) 
      }
   }catch(error){
      logger.error(req.ip + " - " + req.method + " - " + req.originalUrl + " - 修改科目失败")
      res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
   }
}

 /**
 * 根据ID删除一个教师
 * @param {Request} req 
 * @param {Response} res 
 */
exports.deleteSubjectById = async (req,res) => {
   try{
      const { id } = req.query;
      const { modifiedCount } = await deleteSubjectById(id);
      if(modifiedCount > 0){
         res.send(new ResponseMessage(201,"success",{}))
      }else{
         res.send(new ResponseMessage(400,"删除失败,该科目已删除或该用户不存在",{}))
      }
   }catch(error){
      logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 删除科目失败")
      res.send(new ResponseMessage(500,"网络错误",{msg:error.message}));
   }
}