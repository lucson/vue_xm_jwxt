const teacher = require("../model/teacherModel");

/**
 * 查询教师名单
 * @param {String} searchType 
 * @param {String} searchContent 
 * @param {Number} pageIndex 
 * @param {Number} pageSize 
 * @returns 
 */
 exports.getTeacherInfos = async (searchType,searchContent,pageIndex = 1,pageSize = 12) => {
    let searchArr = null;
    if(searchType === "1"){
      searchArr = [{teacherNo:{$regex:new RegExp(searchContent)}}];
    }else if(searchType === "2"){
      searchArr = [{teacherName:{$regex:new RegExp(searchContent)}}];
    }
    const f = searchArr ? { $or: searchArr,isDelete:0} : {isDelete:0};
    return await teacher
      .find(f)
      .skip((pageIndex - 1) * pageSize)
      .limit(pageSize)
      .sort({
        teacherInSchoolDate:-1
      });
  };

 /**
 * 根据ID查找一个教师
 * @param {String} _id 
 * @returns 
 */
exports.findTeacherById = async (_id) => {
  return await teacher.find({_id,isDelete:0})
}

/**
 * 增加一个教师
 * @param {Teacher} tea 
 * @returns 
 */
 exports.addTeacher = async (tea) => {
  return new teacher({
    ...tea
  }).save();
}

/**
 * 修改教师信息
 * @param {Teacher} tea 
 * @returns 
 */
 exports.updateTeacher = async (tea) => {
  return await teacher.updateOne({_id:tea._id},{...tea}) 
}

/**
 * 根据ID删除一个教师
 * @param {String} _id 
 * @returns 
 */
exports.deleteTeacherById = async (_id) => {
  return await teacher.updateOne({_id},{isDelete:1})
}

/**
 * 修改教师个人账户的余额
 * @param {String} studentId 
 * @param {Number} price 
 */
 exports.updateTeacherMoney = async (session,teacherId,price) => {
  return await teacher.updateOne({_id:teacherId},{$inc:{dollar:price}},{session})
}


exports.getTeacherCount = async () => {
  return await teacher.find({}).count();
}