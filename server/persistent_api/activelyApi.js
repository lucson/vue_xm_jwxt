const actively = require("../model/activelyModel");


/**
 * 获取最新的学生活动
 */
exports.getNewsActively = async () => {
  return await actively.find({isDelete:0}).limit(6).sort({ activelyDate: -1 });
};


/**
 * 根据ID删除一个活动
 * @param {String} _id 
 * @returns 
 */
 exports.deleteActivelyById = async (_id) => {
  return await actively.updateOne({_id},{isDelete:1})
}

/**
 * 获取最新的学生活动
 */
 exports.getActivelys = async () => {
  return await actively.find({isDelete:0});
};

/**
 * 添加一个活动
 * @param {Actively} active 
 * @returns 
 */
exports.addNewActively = async (active) => {
  return new actively({
      ...active
  }).save()
}
