const subject = require("../model/subjectModel");

/**
 * 查询科目名单
 * @param {String} searchType 
 * @param {String} searchContent 
 * @param {Number} pageIndex 
 * @param {Number} pageSize 
 * @returns 
 */
 exports.getSubjectInfos = async (searchContent,pageIndex = 1,pageSize = 12) => {
    const f = searchContent ? { $or: [{subjectName:{$regex:new RegExp(searchContent)}}],isDelete:0} : {isDelete:0};
    return await subject
      .find(f)
      .skip((pageIndex - 1) * pageSize)
      .limit(pageSize)
  };


/**
 * 增加一个科目
 * @param {Subject} sub 
 * @returns 
 */
 exports.addSubject = async (sub) => {
  return new subject({
    ...sub
  }).save();
}

/**
 * 修改学院信息
 * @param {Subject} sub 
 * @returns 
 */
 exports.updateSubject = async (sub) => {
  return await subject.updateOne({_id:sub._id},{...sub}) 
}


/**
 * 根据ID删除一个科目
 * @param {String} _id 
 * @returns 
 */
 exports.deleteSubjectById = async (_id) => {
  return await subject.updateOne({_id},{isDelete:1})
}

exports.getSubjectCount = async ()=> {
  return await subject.findOne({isDelete:0}).count();
}

exports.getSubjectById = async (_id) => {
  return await subject.findOne({isDelete:0,_id})
}