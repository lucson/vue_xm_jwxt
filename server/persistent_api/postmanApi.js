const postman = require("../model/postmanModel");

exports.getPostmans = async (_id) => {
     return await postman.find({toId:_id,isDelete:0}).sort({postmanDate:-1});
}

exports.addPostman = async (post) => {
     return new postman({
          ...post
     }).save()
}

exports.updatePostmanStatus = async (_id) => {
     return await postman.updateOne({_id},{readSatus:1})
}