const student = require("../model/studentModel");
const teacher = require("../model/teacherModel");
const dean = require("../model/deanModel");
const admin = require("../model/adminModel");

/**
 * 查询学生是否已登录
 * @param {String} studentNo 
 * @param {String} studentName 
 * @returns {Promise}
 */
exports.studentIsLogin = async (studentNo, studentName) => {
  return await student.findOne({ studentNo, studentName , isDelete:0});
};
/**
 * 修改学生的登录状态
 * @param {String} studentNo 
 * @param {String} studentName 
 * @param {Number} loginStatus 
 * @returns {Promise}
 */
exports.updateStudentLoginStatus = async (studentNo, studentName,loginStatus,_id) => {
  let queryObj = {studentNo,studentName,isDelete:0};
  if(_id){
    queryObj = {_id,isDelete:0}
  }
  return await student.updateOne(queryObj,{loginStatus})
}

/**
 * 查询教师是否已登录
 * @param {String} teacherNo 
 * @param {String} teacherName 
 * @returns {Promise}
 */
exports.teacherIsLogin = async (teacherNo, teacherName) => {
  return await teacher.findOne({ teacherNo, teacherName,isDelete:0 });
};

/**
 * 修改教师的登录状态
 * @param {String} teacherNo 
 * @param {String} teacherName 
 * @param {Number} loginStatus 
 * @returns {Promise}
 */
exports.updateTeacherLoginStatus = async (teacherNo, teacherName,loginStatus,_id) => {
  let queryObj = {teacherNo,teacherName,isDelete:0};
  if(_id){
    queryObj = {_id,isDelete:0}
  }
  return await teacher.updateOne(queryObj,{loginStatus})
}

/**
 * 查询院长是否已登录
 * @param {String} deanNo 
 * @param {String} deanName 
 * @returns {Promise}
 */
exports.deanIsLogin = async (deanNo, deanName) => {
  return await dean.findOne({ deanNo, deanName,isDelete:0 });
};

/**
 * 修改院长的的登录状态
 * @param {String} deanNo 
 * @param {String} deanName 
 * @param {Number} loginStatus 
 * @returns {Promise}
 */
exports.updateDeanLoginStatus = async (deanNo, deanName,loginStatus,_id) => {
  let queryObj = {deanNo,deanName,isDelete:0};
  if(_id){
    queryObj = {_id,isDelete:0}
  }
  return await dean.updateOne(queryObj,{loginStatus})
}

/**
 * 查询校长是否已登录
 * @param {String} name 
 * @param {String} pwd 
 * @returns {Promise} 
 */
exports.adminIsLogin = async (name, pwd) => {
  return await admin.findOne({ name, pwd ,isDelete:0  });
};

/**
 * 修改校长的登录状态
 * @param {String} name 
 * @param {String} pwd 
 * @param {Number} loginStatus 
 * @returns {Promise}
 */
exports.updateAdminLoginStatus = async (name, pwd,loginStatus,_id) => {
  let queryObj = {name,pwd,isDelete:0};
  if(_id){
    queryObj = {_id,isDelete:0}
  }
  console.log(queryObj,{loginStatus});
  return await admin.updateOne(queryObj,{loginStatus})
}