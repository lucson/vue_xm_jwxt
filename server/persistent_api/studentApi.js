const student = require("../model/studentModel");

const getDateTime = require("../utils/getDateTime");

const mongoose = require("mongoose");

/**
 * 获取学生总数
 * @returns Promise
 */
exports.studentCount = async () => {
  return await student.find({ isDelete: 0 }).count();
};

/**
 * 根据时间范围查询新增的学生数量
 * @param {String} cycleType
 * @returns
 */
exports.newAddStudent = async (cycleType) => {
  const [startTime, endTime] = getDateTime(cycleType);
  if (startTime === 0 && endTime === 0) {
    return undefined;
  } else {
    return await student
      .find({
        studentInSchoolDate: { $gte: startTime, $lte: endTime },
        isDelete: 0,
      })
      .count();
  }
};

/**
 * 获取明星学生 - 总成绩排名靠前的
 * @returns Promise
 */
exports.getExcellentStudent = async () => {
  return await student
    .find({ isDelete: 0 })
    .limit(6)
    .sort({ studentTotalScore: -1 });
};

/**
 * 查询学生名单
 * @param {String} searchType
 * @param {String} searchContent
 * @param {Number} pageIndex
 * @param {Number} pageSize
 * @returns
 */
exports.getStudentInfos = async (
  searchType,
  searchContent,
  pageIndex = 1,
  pageSize = 12
) => {
  let searchArr = null;
  if (searchType === "1") {
    searchArr = [{ studentNo: { $regex: new RegExp(searchContent) } }];
  } else if (searchType === "2") {
    searchArr = [{ studentName: { $regex: new RegExp(searchContent) } }];
  }
  const f = searchArr ? { $or: searchArr, isDelete: 0 } : { isDelete: 0 };
  return await student
    .find(f)
    .skip((pageIndex - 1) * pageSize)
    .limit(pageSize)
    .sort({
      studentInSchoolDate: -1,
    });
};

/**
 *
 * @returns 获取所有学生邮件
 */
exports.getStudentsEmail = async () => {
  return await student.find({ isDelete: 0 });
};

/**
 * 根据ID删除一个学生
 * @param {String} _id
 * @returns
 */
exports.deleteStudentById = async (_id) => {
  return await student.updateOne({ _id }, { isDelete: 1 });
};

/**
 * 根据ID查找一个学生
 * @param {String} _id
 * @returns
 */
exports.findStudentById = async (_id) => {
  return await student.aggregate([
    {
      $lookup: {
        from: "departments",
        localField: "studentDepartment",
        foreignField: "departmentNo",
        as: "department",
      },
    },
    {
      $match: { _id: mongoose.Types.ObjectId(_id), isDelete: 0 },
    },
  ]);
};

exports.findStudentByStudentNo = async (studentNo) => {
  return await student.find({
    isDelete: 0,
    studentNo: { $regex: new RegExp(studentNo) },
  });
};

/**
 * 增加一个学生
 * @param {Student} stu
 * @returns
 */
exports.addStudent = async (stu) => {
  return new student({
    ...stu,
  }).save();
};

/**
 * 修改学生信息
 * @param {Student} stu
 * @returns
 */
exports.updateStudent = async (stu) => {
  return await student.updateOne({ _id: stu._id }, { ...stu });
};

/**
 * 修改学生个人账户的余额
 * @param {String} studentId
 * @param {Number} price
 */
exports.updateStudentMoney = async (session, studentId, price) => {
  return await student.updateOne(
    { _id: studentId },
    { $inc: { dollar: price } },
    { session }
  );
};
