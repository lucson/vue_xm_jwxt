// 响应的信息类
module.exports =  function ResponseMessage(code,message,data) {

        this.code = code;
        this.message = message;
        this.data = data;

        ResponseMessage.prototype.setCode = function(code){
            this.code = code;
        }

        ResponseMessage.prototype.getCode = function(){
            return this.code;
        }

        ResponseMessage.prototype.setMessage = function(message){
            this.message = message;
        }

        ResponseMessage.prototype.getMessage = function(){
            return this.message;
        }

        ResponseMessage.prototype.setData = function(data){
            this.data = data;
        }

        ResponseMessage.prototype.getData = function(){
            return this.data;
        }
}