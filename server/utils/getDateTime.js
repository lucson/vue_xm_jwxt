
// 这个文件未做 通用部分的抽离   你们感兴趣可以抽离一下 我这里就不写了(偷懒....)
module.exports = (getDateRangeType) => {
  console.log(getDateRangeType);
    const helperDate  = new Date();
    // 计算今天的时间戳范围
    if(getDateRangeType?.trim() === "nowDay"){
          const commonDatePart = helperDate.getFullYear()+"/"+(+helperDate.getMonth()+1)+"/"+helperDate.getDate();
          const nowDayStartTime  = new Date(commonDatePart + " 00:00:00").getTime();
          const nowDayEndTime = new Date(commonDatePart + " 24:00:00").getTime();
          return [nowDayStartTime,nowDayEndTime]  
    // 计算上个星期的时间戳范围
    }else if(getDateRangeType?.trim() === "lastWeek"){
          let year = helperDate.getFullYear();
          let month = (helperDate.getMonth() + 1)
          const startDay = (helperDate.getDate() - helperDate.getDay() - 6);
          // let startDay = (2 - 6 - 6);
          let endDay = (helperDate.getDate() - helperDate.getDay() - 7 + 7) ;
          if(startDay <= 0) {
             month = month - 1;
             if(+month === 1 || +month === 3 || +month === 5 || +month === 7 || +month === 8 || +month === 10 || +month === 12){
              startDay = 31;
             }else if(+month === 4 || +month === 6 || +month === 9 || +month === 11){
              startDay = 30;
             }else{
               if(year % 4 === 0 && year % 100 !== 0){
                startDay = 29;
               }else{
                startDay = 28;
               }
             }
             startDay = (startDay + helperDate.getDate() - helperDate.getDay() - 6);
          }; 
          if(month <= 0) {    
            year = year - 1;
            month = 12;
          } 
          const lastWeekStart = year + "/" + (month) + "/" + startDay;  
          const lastWeekEnd = year + "/" + (month) + "/" + endDay;
          const lastWeekStartTime  = new Date(lastWeekStart + " 00:00:00").getTime();
          const lastWeekEndTime = new Date(lastWeekEnd + " 24:00:00").getTime();
          return [lastWeekStartTime,lastWeekEndTime]
    // 计算上个月的时间戳范围          
    }else if(getDateRangeType?.trim() === "lastMonth"){
      let year = helperDate.getFullYear();
      let month = (helperDate.getMonth());
      if(month <= 0){
         month = 12;
         year = year - 1;
      }
      const lastMonthStart = year + "/" + (month) + "/" + 1;
      if(+month === 1 || +month === 3 || +month === 5 || +month === 7 || +month === 8 || +month === 10 || +month === 12){
        endDay = 31;
       }else if(+month === 4 || +month === 6 || +month === 9 || +month === 11){
        endDay = 30;
       }else{
         if(year % 4 === 0 && year % 100 !== 0){
          endDay = 29;
         }else{
          endDay = 28;
         }
       }
      const lastMonthEnd = year + "/" + (month) + "/" + endDay;
      const lastMonthStartTime  = new Date(lastMonthStart + " 00:00:00").getTime();
      const lastMonthEndTime = new Date(lastMonthEnd + " 24:00:00").getTime();
      return [lastMonthStartTime,lastMonthEndTime]
    }else{  
        return [0,0];
    } 
} 

