const validate = require("../middleware/validate");
const ResponseMessage = require("../response/message");

const log4js = require("log4js");
const logger = log4js.getLogger();

const { query,body } = require("express-validator");

function checkQueryExists(req,res,msgs = ["缺失查询字符串参数","缺少 query 查询字符串参数"]){
    if(!req.query){
        logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - "+msgs[0])
        res.send(new ResponseMessage(400, msgs[1], {}));
     }
}

exports.addActivelyParamsValidator = async (req,res,next) => {
    if(!req.body){
        logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 缺少请求体")
        res.send(new ResponseMessage(400, "缺少请求体", {}));
    }
    validate([
        body("activelyNo").notEmpty().withMessage("活动编号不能为空"),
        body("activelyTitle").notEmpty().withMessage("活动标题不能为空"),
        body("activelyDate").notEmpty().withMessage("活动日期不能为空"),
    ])(req,res,next)
}

exports.deleteActivelyParamsValidator = async (req,res,next) => {
    checkQueryExists(req,res)
    validate([
        query("_id").notEmpty().withMessage("活动ID不能为空"),
    ])(req,res,next)
}