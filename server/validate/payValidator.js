const validate = require("../middleware/validate");
const ResponseMessage = require("../response/message");

const log4js = require("log4js");
const logger = log4js.getLogger();

const { query,body } = require("express-validator");

function checkQueryExists(req,res,msgs = ["缺失查询字符串参数","缺少 query 查询字符串参数"]){
    if(!req.query){
        logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - "+msgs[0])
        res.send(new ResponseMessage(400, msgs[1], {}));
     }
}

exports.payParamsValidator = async (req, res, next) => {
    checkQueryExists(req,res);
    validate([
        query("orderNo").notEmpty().withMessage("订单号不能为空"),
        query("return_url").notEmpty().withMessage("同步通知地址不能为空"),
        query("notify_url").notEmpty().withMessage("异步通知地址不能为空")
   ])(req,res,next)
}

exports.payHandlerParamsValidator = async (req,res,next) => {
    checkQueryExists(req,res);
    validate([
        query("orderNo").notEmpty().withMessage("订单号不能为空"),
        query("notify_url").notEmpty().withMessage("异步通知地址不能为空"),
        query("fromId").notEmpty().withMessage("付款用户ID不能为空"),
        query("toId").notEmpty().withMessage("收款用户ID不能为空")
   ])(req,res,next)
}