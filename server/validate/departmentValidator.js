const validate = require("../middleware/validate");
const ResponseMessage = require("../response/message");

const log4js = require("log4js");
const logger = log4js.getLogger();

const { query,body } = require("express-validator");

function checkQueryExists(req,res,msgs = ["缺失查询字符串参数","缺少 query 查询字符串参数"]){
    if(!req.query){
        logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - "+msgs[0])
        res.send(new ResponseMessage(400, msgs[1], {}));
     }
}


exports.getTeacherInfosParamsValidator = (req,res,next) => {
    checkQueryExists(req,res);
    (async (callback) => {
       validate([
           query("searchContent").optional().notEmpty().withMessage("搜索的内容不能为空"),
           query("pageIndex").optional().notEmpty().withMessage("当前页不能为空").bail().custom(callback("页码必须是数字")),
           query("pageSize").optional().notEmpty().withMessage("页大小不能为空").bail().custom(callback("页大小必须是数字"))
      ])(req,res,next)
    })((msg) => {
        return async (value)=>{
           if(!/\d+/.test(value)){
               return Promise.reject(msg);
           }
        }
    })
}

exports.deleteDepartmentParamsValidator = async (req,res,next) => {
    checkQueryExists(req,res);
    validate([
        query("id").notEmpty().withMessage("ID 不能为空")
    ])(req,res,next);
}


exports.addTeacherParamsValidator = async (req,res,next) => {
    if(!req.body){
        logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 缺少请求体")
        res.send(new ResponseMessage(400, "缺少请求体", {}));
    }
    validate([
        body("departmentNo").notEmpty().withMessage("学院编号不能为空"),
        body("departmentName").notEmpty().withMessage("学院名不能为空"),
        body("departmentLeader").notEmpty().withMessage("学院院长不能为空"),
        body("departmentCreateDate").notEmpty().withMessage("学院创建日期不能为空"),
        body("studentNumber").notEmpty().withMessage("学生人数不能为空").toInt().isNumeric().withMessage("学生人数必须是数字")
    ])(req,res,next)
}

exports.updateTeacherParamsValidator = async (req,res,next) => {
    if(!req.body){
        logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 缺少请求体")
        res.send(new ResponseMessage(400, "缺少请求体", {}));
    }
    validate([
        body("_id").notEmpty().withMessage("id 不能为空"),
        body("departmentNo").optional().notEmpty().withMessage("学院编号不能为空"),
        body("departmentName").optional().notEmpty().withMessage("学院名不能为空"),
        body("departmentLeader").optional().notEmpty().withMessage("学院院长不能为空"),
        body("departmentCreateDate").optional().notEmpty().withMessage("学院创建日期不能为空"),
        body("studentNumber").optional().notEmpty().withMessage("学生人数不能为空").toInt().isNumeric().withMessage("学生人数必须是数字")
        ])(req,res,next)
}