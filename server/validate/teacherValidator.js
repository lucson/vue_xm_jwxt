const validate = require("../middleware/validate");
const ResponseMessage = require("../response/message");

const log4js = require("log4js");
const logger = log4js.getLogger();

const { query,body } = require("express-validator");

function checkQueryExists(req,res,msgs = ["缺失查询字符串参数","缺少 query 查询字符串参数"]){
    if(!req.query){
        logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - "+msgs[0])
        res.send(new ResponseMessage(400, msgs[1], {}));
     }
}


exports.getTeacherInfosParamsValidator = (req,res,next) => {
    checkQueryExists(req,res);
    (async (callback) => {
       validate([
           query("searchType").optional().exists({checkFalsy:true}).withMessage("搜索类型字段不存在").notEmpty().withMessage("搜索类型不能为空"),
           query("searchContent").optional().notEmpty().withMessage("搜索的内容不能为空"),
           query("pageIndex").optional().notEmpty().withMessage("当前页不能为空").bail().custom(callback("页码必须是数字")),
           query("pageSize").optional().notEmpty().withMessage("页大小不能为空").bail().custom(callback("页大小必须是数字"))
      ])(req,res,next)
    })((msg) => {
        return async (value)=>{
           if(!/\d/.test(value)){
               return Promise.reject(msg);
           }
        }
    })
}

exports.findTeacherParamsValidator = async (req,res,next) =>{
    checkQueryExists(req,res);
    validate([
        query("id").notEmpty().withMessage("ID 不能为空")
    ])(req,res,next);
}


exports.addTeacherParamsValidator = async (req,res,next) => {
    if(!req.body){
        logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 缺少请求体")
        res.send(new ResponseMessage(400, "缺少请求体", {}));
    }
    (async (phoneRuleCheck) => {
        validate([
            body("teacherNo").notEmpty().withMessage("工号不能为空"),
            body("teacherName").notEmpty().withMessage("教师姓名不能为空"),
            body("teacherPhone").notEmpty().withMessage("教师电话号不能为空")
            .bail().custom(phoneRuleCheck("教师手机号不正确")),
            body("teacherEmail").notEmpty().withMessage("邮箱不能为空").isEmail().withMessage("邮箱号不正确"),
            body("teacherSex").notEmpty().withMessage("教师性别不能为空"),
            body("teacherBirthday").notEmpty().withMessage("教师生日不能为空").toInt().isNumeric().withMessage("生日格式必须是时间戳"),
            // body("teacherInSchoolDate").notEmpty().withMessage("教师入职时间不能为空").toInt().isNumeric().withMessage("入职时间格式必须是时间戳"),
            // body("teacherDepartment").notEmpty().withMessage("教师ID不能为空").toInt().isNumeric().withMessage("学院ID必须是数字"),
            // body("teacherSubjectIds").notEmpty().withMessage("教师所学科目不能空"),
            body("teacherSalary").notEmpty().withMessage("教师薪资不能为空").toInt().isNumeric().withMessage("教师薪资必须是数字"),
            body("teacherExperience").notEmpty().withMessage("教师年级不能为空").toInt().isNumeric().withMessage("教师所教年级必须是数字ID"),
            // body("gradeIds").notEmpty().withMessage("教师所教年级不能为空").toInt().isNumeric().withMessage("教师所教年级必须是数字ID"),
        ])(req,res,next)
    })(msg => {
        return async (value)=>{
            if(!/^1(3[0-9]|5[0-3,5-9]|7[1-3,5-8]|8[0-9])\d{8}$/.test(value)){
                return Promise.reject(msg);
            }
         }
    });
}


exports.updateTeacherParamsValidator = async (req,res,next) => {
    if(!req.body){
        logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 缺少请求体")
        res.send(new ResponseMessage(400, "缺少请求体", {}));
    }
    (async (phoneRuleCheck) => {
        validate([
            body("_id").notEmpty().withMessage("id不能为空"),
            body("teacherNo").optional().notEmpty().withMessage("工号不能为空"),
            body("teacherName").optional().notEmpty().withMessage("教师姓名不能为空"),
            body("teacherPhone").optional().notEmpty().withMessage("教师电话号不能为空")
            .bail().custom(phoneRuleCheck("教师手机号不正确")),
            body("teacherEmail").optional().notEmpty().withMessage("邮箱不能为空").isEmail().withMessage("邮箱号不正确"),
            body("teacherSex").optional().notEmpty().withMessage("教师性别不能为空"),
            body("teacherBirthday").optional().notEmpty().withMessage("教师生日不能为空").toInt().isNumeric().withMessage("生日格式必须是时间戳"),
            body("teacherInSchoolDate").optional().notEmpty().withMessage("教师入职时间不能为空").toInt().isNumeric().withMessage("入职时间格式必须是时间戳"),
            body("teacherDepartment").optional().notEmpty().withMessage("教师ID不能为空").toInt().isNumeric().withMessage("学院ID必须是数字"),
            body("teacherSubjectIds").optional().notEmpty().withMessage("教师所学科目不能空"),
            body("teacherSalary").optional().notEmpty().withMessage("教师薪资不能为空").toInt().isNumeric().withMessage("教师薪资必须是数字"),
            body("teacherExperience").optional().notEmpty().withMessage("教师年级不能为空").toInt().isNumeric().withMessage("教师所教年级必须是数字ID"),
            body("gradeIds").optional().notEmpty().withMessage("教师所教年级不能为空").toInt().isNumeric().withMessage("教师所教年级必须是数字ID"),
            ])(req,res,next)
    })(msg => {
        return async (value)=>{
            if(!/^1(3[0-9]|5[0-3,5-9]|7[1-3,5-8]|8[0-9])\d{8}$/.test(value)){
                return Promise.reject(msg);
            }
         }
    });
}

exports.deleteTeacherParamsValidator = async (req,res,next) => {
    checkQueryExists(req,res);
    validate([
        query("id").notEmpty().withMessage("ID 不能为空")
    ])(req,res,next);
}