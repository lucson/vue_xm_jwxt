const validate = require("../middleware/validate");
const ResponseMessage = require("../response/message");

const log4js = require("log4js");
const logger = log4js.getLogger();

const { query,body } = require("express-validator");

function checkQueryExists(req,res,msgs = ["缺失查询字符串参数","缺少 query 查询字符串参数"]){
    if(!req.query){
        logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - "+msgs[0])
        res.send(new ResponseMessage(400, msgs[1], {}));
     }
}

exports.newAddStudentParamsValidator = (req, res, next) => {
    checkQueryExists(req,res);
    validate([
         query("cycleType").notEmpty().withMessage("时间范围类型不能为空"),
    ])(req,res,next)
}

exports.findStudentByOrderNoParamsValidator = (req, res, next) => {
    checkQueryExists(req,res);
    validate([
         query("studentNo").notEmpty().withMessage("学生学号不能为空"),
    ])(req,res,next)
}


exports.getStudentInfosParamsValidator = (req,res,next) => {
     checkQueryExists(req,res);
     (async (callback) => {
        validate([
            query("searchType").optional().exists({checkFalsy:true}).withMessage("搜索类型字段不存在").notEmpty().withMessage("搜索类型不能为空"),
            query("searchContent").optional().notEmpty().withMessage("搜索的内容不能为空"),
            query("pageIndex").optional().notEmpty().withMessage("当前页不能为空").toInt().bail().custom(callback("页码必须是数字")),
            query("pageSize").optional().notEmpty().withMessage("页大小不能为空").toInt().bail().custom(callback("页大小必须是数字"))
       ])(req,res,next)
     })((msg) => {
         return async (value)=>{
            if(!/\d/.test(value)){
                return Promise.reject(msg);
            }
         }
     })
}


exports.deleteStudentParamsValidator = async (req,res,next) => {
    checkQueryExists(req,res);
    validate([
        query("id").notEmpty().withMessage("ID 不能为空")
    ])(req,res,next);
}


exports.findStudentParamsValidator = async (req,res,next) =>{
    checkQueryExists(req,res);
    validate([
        query("id").notEmpty().withMessage("ID 不能为空")
    ])(req,res,next);
}

exports.addStudentParamsValidator = async (req,res,next) => {
    if(!req.body){
        logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 缺少请求体")
        res.send(new ResponseMessage(400, "缺少请求体", {}));
    }
    (async (phoneRuleCheck) => {
        validate([
            body("studentNo").notEmpty().withMessage("学号不能为空"),
            body("studentName").notEmpty().withMessage("学生姓名不能为空"),
            body("studentPhone").notEmpty().withMessage("学生电话号不能为空")
            .bail().custom(phoneRuleCheck("学生手机号不正确")),
            body("studentEmail").notEmpty().withMessage("邮箱不能为空").isEmail().withMessage("邮箱号不正确"),
            body("studentSex").notEmpty().withMessage("学生性别不能为空"),
            body("studentBirthday").notEmpty().withMessage("学生生日不能为空").toInt().isNumeric().withMessage("生日格式必须是时间戳"),
            body("studentInSchoolDate").notEmpty().withMessage("学生入学时间不能为空").toInt().isNumeric().withMessage("入学时间格式必须是时间戳"),
            body("studentDepartment").notEmpty().withMessage("学院ID不能为空").toInt().isNumeric().withMessage("学院ID必须是数字"),
            // body("studentSubjectsIds").notEmpty().withMessage("学生所学科目不能空"),
            // body("studentGradeId").notEmpty().withMessage("学生年级不能为空").toInt().isNumeric().withMessage("学生年级必须是数字ID"),
            ])(req,res,next)
    })(msg => {
        return async (value)=>{
            if(!/^1(3[0-9]|5[0-3,5-9]|7[1-3,5-8]|8[0-9])\d{8}$/.test(value)){
                return Promise.reject(msg);
            }
         }
    });
}

exports.updateStudentParamsValidator = async (req,res,next) => {
    if(!req.body){
        logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - 缺少请求体")
        res.send(new ResponseMessage(400, "缺少请求体", {}));
    }
    (async (phoneRuleCheck) => {
        validate([
            body("_id").notEmpty().withMessage("id不能为空"),
            body("studentNo").optional().notEmpty().withMessage("学号不能为空"),
            body("studentName").optional().notEmpty().withMessage("学生姓名不能为空"),
            body("studentPhone").optional().notEmpty().withMessage("学生电话号不能为空")
            .bail().custom(phoneRuleCheck("学生手机号不正确")),
            body("studentEmail").optional().notEmpty().withMessage("邮箱不能为空").isEmail().withMessage("邮箱号不正确"),
            body("studentSex").optional().notEmpty().withMessage("学生性别不能为空"),
            body("studentBirthday").optional().notEmpty().withMessage("学生生日不能为空").toInt().isNumeric().withMessage("生日格式必须是时间戳"),
            body("studentInSchoolDate").optional().notEmpty().withMessage("学生入学时间不能为空").toInt().isNumeric().withMessage("入学时间格式必须是时间戳"),
            body("studentDepartment").optional().notEmpty().withMessage("学院ID不能为空").toInt().isNumeric().withMessage("学院ID必须是数字"),
            body("studentSubjectsIds").optional().notEmpty().withMessage("学生所学科目不能空"),
            body("studentGradeId").optional().notEmpty().withMessage("学生年级不能为空").toInt().isNumeric().withMessage("学生年级必须是数字ID"),
            ])(req,res,next)
    })(msg => {
        return async (value)=>{
            if(!/^1(3[0-9]|5[0-3,5-9]|7[1-3,5-8]|8[0-9])\d{8}$/.test(value)){
                return Promise.reject(msg);
            }
         }
    });
}