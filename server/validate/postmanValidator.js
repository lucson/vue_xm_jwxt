const validate = require("../middleware/validate");
const ResponseMessage = require("../response/message");

const log4js = require("log4js");
const logger = log4js.getLogger();

const { query,body } = require("express-validator");

function checkQueryExists(req,res,msgs = ["缺失查询字符串参数","缺少 query 查询字符串参数"]){
    if(!req.query){
        logger.warn(req.ip + " - " + req.method + " - " + req.originalUrl + " - "+msgs[0])
        res.send(new ResponseMessage(400, msgs[1], {}));
     }
}

exports.getPoatmanParamsValidator = async (req, res, next) => {
    checkQueryExists(req,res);
    validate([
        query("_id").notEmpty().withMessage("用户ID不能为空"),
   ])(req,res,next)
}

exports.addPoatmanParamsValidator = async (req, res, next) => {
    checkQueryExists(req,res);
    validate([
        body("postmanNo").notEmpty().withMessage("邮件编号不能为空"),
        body("postmanTitle").notEmpty().withMessage("邮件标题不能为空"),
        body("postmanContent").notEmpty().withMessage("邮件内容不能为空"),
        body("postmanDate").notEmpty().withMessage("邮件发送日期不能为空"),
        body("toId").notEmpty().withMessage("接受者ID不能为空"),
        body("from").notEmpty().withMessage("发送者姓名不能为空"),
        body("fromId").notEmpty().withMessage("发送者ID不能为空"),
   ])(req,res,next)
}

exports.updateStatusPoatmanParamsValidator = async (req, res, next) => {
    checkQueryExists(req,res);
    validate([
        body("_id").notEmpty().withMessage("邮件ID不能为空"),
   ])(req,res,next)
}