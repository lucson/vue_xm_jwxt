export default {
    namespaced:true,
    state:() => ({
         loginType:Number(localStorage.getItem("jwxt_loginType")) || 1,
         token:localStorage.getItem("jwxt_token") || "",
         userInfo:JSON.parse(localStorage.getItem("jwxt_userInfo")) || {},
         expire:0
    }),
    actions:{
        setToken(store,token){
            store.commit("SET_TOKEN",token)
        },
        setUserInfo(store,userInfo){
            store.commit("SET_USERINFO",userInfo)
        },
        setLoginType(store,loginType){
            store.commit("SET_LOGINTYPE",loginType)
        },
        setExpire(store,expire){
            store.commit("SET_EXPIRE",expire)
        }
    },  
    mutations:{
        SET_TOKEN(state,token){
            state.token = token;
        },
        SET_USERINFO(state,userInfo){
            state.userInfo = userInfo;
        },
        SET_LOGINTYPE(state,loginType){
            state.loginType = loginType;
        },
        SET_EXPIRE(state,expire){
            state.expire = expire;
        }
    }
}