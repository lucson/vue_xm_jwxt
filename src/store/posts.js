export default {
    namespaced:true,
    state:() => ({
         posts:[],
    }),
    actions:{
        setPosts(store,posts){
            store.commit("SET_POSTS",posts);
        }
    },  
    mutations:{
        SET_POSTS(state,posts){
             state.posts = posts;
        }
    }
}