import Vue from "vue";
import App from "./App.vue";
import router from "@/router";
import store from "@/store";

import SocketIO from "socket.io-client";
import VueSocketIO from "vue-socket.io";

import request from "@/api/request";
import { loginAuthApi } from "@/api/api_constant";

// import customTag from "./plugins/custom-tag";

// Vue.use(customTag)

import Tag from "vue2-tag";

Vue.use(Tag);

import ElementUi from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import "nprogress/nprogress.css";

import NavsMinins from "@/mixins/navs";
import ExportMixin from "@/mixins/export_excel"
import ToggleContent from "@/components/ToggleContent";
import PanelDesc from "@/components/PanelDesc";
// import { Button, Select } from 'element-ui';
import "element-ui/lib/theme-chalk/display.css";
// Vue.component(Button.name, Button);
// Vue.component(Select.name, Select);

Vue.use(
  new VueSocketIO({
    debug: true,
    connection: SocketIO("http://localhost:3000/pay"),
  })
);

Vue.use(ElementUi);
Vue.mixin(NavsMinins);
Vue.mixin(ExportMixin);
Vue.component("toggle-content", ToggleContent);
Vue.component(PanelDesc.name, PanelDesc);

Vue.filter("formatNowDate", (v) => {
  if (typeof v === "number") {
    return new Date(v).toLocaleString();
  } else if (typeof v === "object") {
    return v.toLocaleString();
  }
});

Vue.config.productionTip = false;

router.beforeEach(async (to, form, next) => {
  const token = store.state.user.token;
  const userInfo = store.state.user.userInfo;
  // 判断是否登录 跳转到 login 页
  if (to.meta.isAuth) {
    // 验证登录态
    if (token === "" || JSON.stringify(userInfo) === "{}") {
      return router.replace({
        path: "/login",
      });
    }
    const resInfo = await request.post(loginAuthApi, {
      loginType: store.state.user.loginType,
      id: userInfo._id,
    });
    if (resInfo.isExpireToken) {
      localStorage.removeItem("jwxt_token");
      localStorage.removeItem("jwxt_userInfo");
      localStorage.removeItem("jwxt_loginType");
      store.dispatch("user/setToken", "");
      store.dispatch("user/setUserInfo", {});
      store.dispatch("user/setLoginType", 0);
      return;
    }  
  } else {
    if (token !== "" && JSON.stringify(userInfo) !== "{}") {
      if (to.path === "/login") {
        return router.replace({
          path: "/",
        });
      }
    }
  }

  // 设置面包屑路径
  router.navs = to.meta.navs;
  next();
});

router.afterEach((to, from) => {
  document.title = to.meta.title;
});

new Vue({
  router,
  store,
  render: (h) => h(App),
  beforeCreate() {
    Vue.prototype.$bus = this;
  },
}).$mount("#app");
