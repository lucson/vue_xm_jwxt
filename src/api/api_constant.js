// 登录模块

// POST

export const loginApi = "/login"

export const loginAuthApi = "/auth"

// GET

export const loginOutApi = "/logout"

// 控制台模块

// GET

export const studentCountApi = "/student/count"

export const departmentCountApi = "/department/count"

export const totalTradePriceApi = "/order/total_price"

export const findTotalPriceRangeApi = "/order/cycle_trade_price"

export const findNewAddStudentCountApi = "/student/new_add_student"

export const getExecllentStudentApi = "/student/get_excellent_student"

export const getNewActivelyApi = "/actively/get_news_actively"

// 学生模块

// GET

export const getStudentListApi = "/student/get_student_infos";

export const findStudentByIdApi = "/student/get_student"

export const findStudentByStudentNoApi = "/student/student"

// DELETE

export const deleteStudentApi = "/student/delete_student"

// PUT

export const updateStudentApi = "/student/update_student";

// POST

export const addStudentApi = "/student/add_student"


// 教师模块

// GET

export const getTeacherListApi = "/teacher/get_teacher_infos";

export const getTeacherApi = "/teacher/get_teacher";

export const getTeacherCountApi = "/teacher/get_teacher_count"

// POST

export const addTeacherApi = "/teacher/add_teacher";

// DELETE

export const deleteTeacherApi = "/teacher/delete_teacher";

// PUT

export const updateTeacherApi = "/teacher/update_teacher"

// 学院模块

//  GET

export const getDepartmentsApi = "/department/get_departments";

export const getDeansApi = "/dean/get_deans";

export const getDepartmentByIdApi = "/department/get_department"

// POST

export const addDepartmentApi = "/department/add_department";

// PUT

export const updateDepartmentApi = "/department/update_department"

// DELETE

export const deleteDepartmentApi = "/department/delete_department"

// 科目模块

// GET

export const getSubjectsApi = "/subject/get_teacher_infos"; 

export const getSubjectCountApi = "/subject/get_subject_count"

export const getSubjectByIdApi = "/subject/get_subject"

// POST

export const addSubjectApi = "/subject/add_subject"

// PUT

export const updateSubjectApi = "/subject/update_subject";

// DELETE

export const deleteSubjectApi = "/subject/delete_subject"

// 账户模块

// GET

export const getOrdersApi = "/order/get_orders"

export const payApi = "/pay"

export const getOrderByOrderNoApi = "/order/get_order"

export const getOrderCountApi = "/order/order_count"

// DELETE

export const deleteOrderApi = "/order/delete_order"

// POST

export const addOrder = "/order/add_order"

// 邮件箱模块

// GET

export const getPostmanApi = "/postman/get_postmans"

// PUT

export const updatePostmanStatusApi = "/postman/update_postman_status"

// POST

export const addPostmanApi = "/postman/add_postman"

// 活动模块

// POST

export const addActivelyApi = "/actively/add_actively"

// DELETE

export const deleteActivelyApi = "/actively/delete_actively"

// PUT

export const getActivelysApi = "/actively/get_activelys"

