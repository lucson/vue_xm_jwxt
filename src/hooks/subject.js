import request from "@/api/request";

import {
  getSubjectsApi,
  addSubjectApi,
  updateSubjectApi,
  deleteSubjectApi,
  getSubjectCountApi,
  getSubjectByIdApi
} from "@/api/api_constant";

export const useGetSubjects = async (searchContent, pageIndex, pageSize) => {
  return (
    await request.get(getSubjectsApi, {
      params:{
        searchContent,
        pageIndex,
        pageSize
      },
    })
  ).data;
};

export const useGetSubjectCount = async () => {
  return (
    await request.get(getSubjectCountApi)
  ).data
}

export const useAddSubject = async (subject) => {
    return (
        await request.post(addSubjectApi,subject)
    ).data
}

export const useUpdateSubject = async (subject) => {
    return (
        await request.put(updateSubjectApi,subject)
    ).data
}

export const useDeleteSubject = async (id) => {
    return (
        await request.delete(deleteSubjectApi,{
            params:{
                id
            }
        })
    )
}

export const useGetSubject = async (id) => {
   return (
     await request.get(getSubjectByIdApi,{
       params:{
         id
       }
     })
   ).data
}