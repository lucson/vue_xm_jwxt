import request from "@/api/request";

import {
  getTeacherListApi,
  getTeacherApi,
  getTeacherCountApi,
  addTeacherApi,
  deleteTeacherApi,
  updateTeacherApi,
} from "@/api/api_constant";

export const useGetTeacher = async (id) => {
   return (
     await request.get(getTeacherApi,{
        params:{
          id
        }
     })
   ).data
}

export const useGetTeacherList = async (
  searchType,
  searchContent,
  pageIndex,
  pageSize
) => {
  return (
    await request.get(getTeacherListApi, {
      params: {
        searchType,
        searchContent,
        pageIndex,
        pageSize,
      },
    })
  ).data;
};

export const useGetTeacherCount = async () => {
  return (await request.get(getTeacherCountApi)).data;
};


export const useDeleteTeacher = async (id) => {
    return (
      await request.delete(deleteTeacherApi, {
        params: {
          id,
        },
      })
    ).data;
};

export const useUpdateTeacher = async (teacher) => {
  return (
    await request.put(updateTeacherApi,teacher)
  ).data
}

export const useAddTeacher = async (teacher) => {
  return (
    await request.post(addTeacherApi,teacher)
  ).data;
}

