import request from "@/api/request";

import {
  getStudentListApi,
  deleteStudentApi,
  findStudentByIdApi,
  updateStudentApi,
  addStudentApi
} from "@/api/api_constant";

export const useGetStudents = async (
  searchType,
  searchContent,
  pageIndex,
  pageSize
) => {
  return (
    await request.get(getStudentListApi, {
      params: {
        searchType,
        searchContent,
        pageIndex,
        pageSize,
      },
    })
  ).data;
};

export const useDeleteStudent = async (id) => {
  return (
    await request.delete(deleteStudentApi, {
      params: {
        id,
      },
    })
  ).data;
};

export const useGetStudent = async (id) => {
  return (
    await request.get(findStudentByIdApi, {
      params: {
        id,
      },
    })
  ).data;
};

export const useUpdateStudent = async (student) => {
  return (await request.put(updateStudentApi, student)).data;
};

export const useAddStudent = async (student) => {
  return (
    await request.post(addStudentApi,student)
  ).data;
};
