import request from "@/api/request";
import {
  addActivelyApi,
  deleteActivelyApi,
  getActivelysApi,
} from "@/api/api_constant";

export const useGetActivelys = async () => {
  return (await request.get(getActivelysApi)).data;
};

export const useDeleteActively = async (_id) => {
  return (
    await request.delete(deleteActivelyApi, {
      params: {
        _id,
      },
    })
  ).data;
};

export const useAddActively = async (actively) => {
  return (await request.post(addActivelyApi, actively)).data;
};
