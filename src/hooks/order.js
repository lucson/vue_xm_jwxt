import request from "@/api/request";

import {
    getOrdersApi,
    payApi,
    getOrderByOrderNoApi,
    deleteOrderApi,
    addOrder,
    getOrderCountApi,
    findStudentByStudentNoApi
} from "@/api/api_constant";

export const useGetOrderCount = async () => {
    return (
        await request.get(getOrderCountApi)
    ).data
}

export const useDeleteOrder = async (id) => {
    return (
        await request.delete(deleteOrderApi,{
            params:{
                id
            }
        })
    ).data
}

export const useGetOrders = async (searchContent,pageIndex,pageSize) => {
    return (
        await request.get(getOrdersApi,{
            params:{
                searchContent,
                pageIndex,
                pageSize
            }
        })
    ).data;
}

export const useAddOrder = async (order) => {
    return (
        await request.post(addOrder,order)
    ).data;
}

export const usePay = async (orderNo,return_url,notify_url) => {
    return (
        await request.get(payApi,{
            params:{
                orderNo,
                return_url,
                notify_url
            }
        })
    ).data
}


export const useGetOrder = async (orderNo) => {
    return (
        await request.get(getOrderByOrderNoApi,{
            params:{
                orderNo
            }
        })
    ).data
}

export const useFindStudentByStudentNo = async (studentNo) => {
    return (
        await request.get(findStudentByStudentNoApi,{
            params:{
                studentNo
            }
        })
    ).data
}