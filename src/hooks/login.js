
export const useLoginLogic =  async (loginComponent,resInfo,loadingInstance1) => {
    if(+resInfo.code === 201 && resInfo.message === "success"){
        loginComponent.$message.success('登录成功');
        loadingInstance1.close();
        loginComponent.setToken(resInfo.data.token)
        loginComponent.setUserInfo(resInfo.data.userInfo)
        localStorage.setItem("jwxt_token",resInfo.data.token)
        localStorage.setItem("jwxt_userInfo",JSON.stringify(resInfo.data.userInfo))
        loginComponent.$router.replace("/")
    }else{
        loginComponent.$message.error(resInfo.message);
        loadingInstance1.close();
    }
}

export const useLogOut = async (vueComponent,resInfo) => {
    console.log(resInfo);
    if(resInfo.code === 201 && resInfo.message === "success"){
           localStorage.removeItem("jwxt_token");
           localStorage.removeItem("jwxt_userInfo");
           localStorage.removeItem("jwxt_loginType");
           vueComponent.setExpire(0);
           vueComponent.setToken("");
           vueComponent.setUserInfo({});
           vueComponent.setLoginType(1);
           vueComponent.$message.success("登出成功")
           vueComponent.$router.replace("/login")
    }else{
        vueComponent.$message.error(resInfo.message)
    }
}