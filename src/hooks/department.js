import request from "@/api/request";

import {
  getDepartmentsApi,
  getDeansApi,
  getDepartmentByIdApi,
  addDepartmentApi,
  updateDepartmentApi,
  deleteDepartmentApi,
  departmentCountApi
} from "@/api/api_constant";


export const useAddDepartment = async (department) => {
   return (
     await request.post(addDepartmentApi,department)
   ).data
}

export const useUpdateDepartment = async (department) => {
  return (
    await request.put(updateDepartmentApi,department)
  ).data
}

export const useGetDepartmentCount = async () => {
  return (
    await request.get(departmentCountApi)
  ).data
}

export const useDeleteDepartment = async (id) => {
  return (
    await request.delete(deleteDepartmentApi,{
      params:{
        id
      }
    })
  )
}

export const useGetDepartments = async (
  searchContent,
  pageIndex,
  pageSize
) => {
  return (
    await request.get(getDepartmentsApi, {
      params: {
        searchContent,
        pageIndex,
        pageSize,
      },
    })
  ).data;
};


export const useGetDeans = async () => {
  return (
     await request.get(getDeansApi)
   ).data;
}

export const useGetDepartment = async (id) => {
  return (
    await request.get(getDepartmentByIdApi,{
      params:{
        id
      }
    })
  ).data
}