import request from "@/api/request";

import { updatePostmanStatusApi, addPostmanApi } from "@/api/api_constant";

export const useUpdatePostmanStatus = async (_id) => {
  return (
    await request.put(updatePostmanStatusApi,{
        _id
    })
  ).data;
};

export const useAddPostmanApi = async (postman) => {
  return (await request.post(addPostmanApi, postman)).data;
};
