import request from "@/api/request";
import {
  studentCountApi,
  departmentCountApi,
  totalTradePriceApi,
  findTotalPriceRangeApi,
  findNewAddStudentCountApi,
  getExecllentStudentApi,
  getNewActivelyApi,
  getPostmanApi
} from "@/api/api_constant";

export const useGetStudentCount = async () => {
  return (await request.get(studentCountApi)).data.data;
};

export const useGetDepartmentCount = async () => {
  return (await request.get(departmentCountApi)).data.data;
};

export const useGetOrderTotalPrice = async () => {
  return (await request.get(totalTradePriceApi)).data.data;
};

export const useGetFindTotalPrice = async (cycleType) => {
  return (
    await request.get(findTotalPriceRangeApi, {
      params: {
        cycleType,
      },
    })
  ).data;
};

export const useGetNewAddStudentCount = async (cycleType) => {
  return (
    await request.get(findNewAddStudentCountApi, {
      params: {
        cycleType,
      },
    })
  ).data;
};

export const useGetExcellentStudent = async () => {
   return (await request.get(getExecllentStudentApi)).data
}

export const useGetNewActively = async () => {
  return (await request.get(getNewActivelyApi)).data
}

export const useGetPostman = async (id) => {
  return (await request.get(getPostmanApi,{
    params:{
      _id:id
    }
  })).data
}