export const throttle = (fn, delay) => {
  var flag = true;
  return function (e) {
    if (flag) {
      flag = false;
      setTimeout(
        function () {
          flag = true;
          fn.bind(this)(e);
        }.bind(this),
        delay
      );
    }
  };
};

export const isMobile = () => {
   return /android|ios/gi.test(navigator.userAgent.toLowerCase());
};

