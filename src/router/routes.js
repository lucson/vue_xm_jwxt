export default [
  {
    path: "/",
    redirect: "/control",
    meta: { title: "控制中心", navs: ["控制中心"] },
  },
  {
    path: "/control",
    name: "control",
    components: {
      eqPage: () => import("@/pages/ControlBoard"),
    },
    meta: { isAuth: true, title: "控制中心", navs: ["控制中心"] },
  },
  // {
  //   path: "/department",
  //   name: "department",
  //   component: () => import("@/pages/Department"),
  //   meta: { isAuth: true, title: "学院管理", navs: ["学院管理"] },
  // },
  {
    path: "/department",
    name: "department",
    components: {
      eqPage:  () => import("@/pages/Department"),
    },
    meta: { isAuth: true, title: "学院管理", navs: ["学院管理"] },
    children: [
      {
        path: "add",
        name: "departmentAdd",
        components: {
          eqPage: () => import("@/pages/Department/DepartmentAdd"),
        },
        meta: {
          isAuth: true,
          title: "学院管理 - 学院添加",
          navs: ["学院管理", "学院添加"],
        },
      },
      {
        path: "edit/:id?",
        name: "departmentEdit",
        components: {
          eqPage: () => import("@/pages/Department/DepartmentEdit"),
        },
        meta: {
          isAuth: true,
          title: "学院管理 - 学院编辑",
          navs: ["学院管理", "学院编辑"],
        },
      },
    ],
  },
  {
    path: "/student",
    name: "student",
    components: {
      eqPage: () => import("@/pages/Student"),
    },
    meta: { isAuth: true, title: "学生管理", navs: ["学生管理"] },
    children: [
      {
        path: "view",
        name: "studentView",
        components: {
          eqPage: () => import("@/pages/Student/StudentView"),
        },
        meta: {
          isAuth: true,
          title: "学生管理 - 学生视图",
          navs: ["学生管理", "学生视图"],
        },
      },
      {
        path: "add",
        name: "studentAdd",
        components: {
          eqPage: () => import("@/pages/Student/StudentAdd"),
        },
        meta: {
          isAuth: true,
          title: "学生管理 - 添加学生",
          navs: ["学生管理", "添加学生"],
        },
      },
      {
        path: "edit",
        name: "studentEdit",
        components: {
          eqPage: () => import("@/pages/Student/StudentEdit"),
        },
        meta: {
          isAuth: true,
          title: "学生管理 - 编辑学生",
          navs: ["学生管理", "编辑学生"],
        },
      },
    ],
  },
  {
    path: "/subject",
    name: "subject",
    components: {
      eqPage:  () => import("@/pages/Subject"),
    },
    meta: { isAuth: true, title: "科目管理", navs: ["科目管理"] },
    children: [
      {
        path: "add",
        name: "subjectAdd",
        components: {
          eqPage:() => import("@/pages/Subject/SubjectAdd"),
        },
        meta: {
          isAuth: true,
          title: "科目管理 - 添加科目",
          navs: ["科目管理", "添加科目"],
        },
      },
      {
        path: "edit/:id?",
        name: "subjectEdit",
        // props:true, // 用了命名路由视图 这个功能会失效
        components: {
          eqPage: () => import("@/pages/Subject/SubjectEdit"),
        },
        meta: {
          isAuth: true,
          title: "科目管理 - 编辑科目",
          navs: ["科目管理", "编辑科目"],
        },
      },
    ],
  },
  {
    path: "/teacher",
    name: "teacher",
    components: {
      eqPage:() => import("@/pages/Teacher"),
    },
    meta: { isAuth: true, title: "教师管理", navs: ["教师管理"] },
    children: [
      {
        path: "view",
        name: "teacherView",
        components: {
          eqPage:  () => import("@/pages/Teacher/TeacherView"),
        },
        meta: {
          isAuth: true,
          title: "教师管理 - 教师视图",
          navs: ["教师管理", "教师视图"],
        },
      },
      {
        path: "add",
        name: "teacherAdd",
        components: {
          eqPage: () => import("@/pages/Teacher/TeacherAdd"),
        },
        meta: {
          isAuth: true,
          title: "教师管理 - 添加教师",
          navs: ["教师管理", "添加教师"],
        },
      },
      {
        path: "edit",
        name: "teacherEdit",
        components: {
          eqPage:  () => import("@/pages/Teacher/TeacherEdit"),
        },
        meta: {
          isAuth: true,
          title: "教师管理 - 编辑教师",
          navs: ["教师管理", "编辑教师"],
        },
      },
    ],
  },
  {
    path: "/account",
    name: "account",
    components: {
      eqPage: () => import("@/pages/Account"),
    },
    meta: { isAuth: true, title: "账户管理", navs: ["账户管理"] },
    children: [
      {
        path: "spend",
        name: "accountSpend",
        components: {
          eqPage: () => import("@/pages/Account/AccountSpend"),
        },
        meta: {
          isAuth: true,
          title: "账户管理 - 账户花费",
          navs: ["账户管理", "账户花费"],
        },
      },
      {
        path: "charge",
        name: "accountCharge",
        components: {
          eqPage:() => import("@/pages/Account/AccountCharge"),
        },
        meta: {
          isAuth: true,
          title: "账户管理 - 账户收费",
          navs: ["账户管理", "账户收费"],
        },
      },
      {
        path: "trade",
        name: "accountTrade",
        components: {
          eqPage: () => import("@/pages/Account/AccountTrade"),
        },
        meta: {
          isAuth: true,
          title: "账户管理 - 账户交易",
          navs: ["账户管理", "账户交易"],
        },
      },
    ],
  },
  {
    path: "/postman",
    name: "postman",
    components: {
      eqPage: () => import("@/pages/Postman"),
    },
    meta: { isAuth: true, title: "邮件", navs: ["收件箱"] },
  },
  {
    path: "/profile",
    name: "profile",
    components: {
      eqPage: () => import("@/pages/Profile"),
    },
    meta: { isAuth: true, title: "个人简历", navs: ["个人简历"] },
  },
  {
    path: "/testlist",
    name: "testlist",
    components: {
      eqPage: () => import("@/pages/TestList"),
    },
    meta: { isAuth: true, title: "考试清单", navs: ["考试清单"] },
  },
  {
    path: "/actively",
    name: "actively",
    components: {
      eqPage: () => import("@/pages/Actively"),
    },
    meta: { isAuth: true, title: "活动管理", navs: ["活动管理"] },
  },
  {
    path: "/time",
    name: "time",
    components: {
      eqPage:  () => import("@/pages/Time"),
    },
    meta: { isAuth: true, title: "时间表", navs: ["时间表"] },
  },
  {
    path: "/libary",
    name: "libary",
    components: {
      eqPage: () => import("@/pages/Libary"),
    },
    meta: { isAuth: true, title: "图书馆", navs: ["图书馆"] },
  },
  {
    path: "/login",
    name: "login",
    components: {
      eqLoginComponent: () => import("@/pages/Login"),
    },
    meta: { title: "登录" },
  },
  {
    path: "*",
    name: "NotFound",
    components: {
      eqLoginComponent: () => import("@/pages/NotFound"),
      eqPage: () => import("@/pages/NotFound")
    },
    meta: { title: "页面丢失啦~" },
  },
];
