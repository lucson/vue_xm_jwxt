const validateId = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("学号不能为空"));
  }
  callback();
};
const validateName = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("姓名不能为空"));
  } else if (value.length <= 1) {
    return callback(new Error("请填写正确的名字"));
  }
  callback();
};
const validatePhone = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("电话不能为空"));
  } else if (value.length !== 11) {
    return callback(new Error("请填写正确的电话号"));
  }
  callback();
};

const validateEmail = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("邮箱不能为空"));
  } else if (
    !/^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/.test(value)
  ) {
    return callback(new Error("请填写正确的邮箱号"));
  }
  callback();
};


const validateBirthday = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("生日不能为空"));
  }
  callback();
};
const validateClass = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("班级不能为空"));
  }
  callback();
};
const validateInSchoolDate = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("入学日期不能为空"));
  }
  callback();
};
const validateDepartment = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("院系不能为空"));
  }
  callback();
};
const validateNowAddress = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("地址不能为空"));
  } else if (value.length < 2) {
    return callback(new Error("请填写正确的地址"));
  }
  callback();
};

const validateHomeAddress = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("家乡地址不能为空"));
  } else if (value.length < 2) {
    return callback(new Error("请填写正确的地址"));
  }
  callback();
};

const validateParentName = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("父母姓名不能为空"));
  } else if (value.length <= 1) {
    return callback(new Error("请填写正确的姓名"));
  }
  callback();
};
const validateParentPhone = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("父母电话不能为空"));
  } else if (value.length !== 11) {
    return callback(new Error("请填写正确的电话号"));
  }
  callback();
};
const validateParentJob = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("父母职业不能为空"));
  } else if (value.length < 2) {
    return callback(new Error("请填写正确的职业"));
  }
  callback();
};


export default  {
    studentNo: [{ validator: validateId, trigger: "blur" }],
    studentName: [{ validator: validateName, trigger: "blur" }],
    studentPhone: [{ validator: validatePhone, trigger: "blur" }],
    studentEmail: [{ validator: validateEmail, trigger: "blur" }],
    studentBirthday: [{ validator: validateBirthday, trigger: "blur" }],
    // class: [{ validator: validateClass, trigger: "blur" }],
    studentInSchoolDate: [{ validator: validateInSchoolDate, trigger: "blur" }],
    // department: [{ validator: validateDepartment, trigger: "blur" }],
    studentNowAddress: [{ validator: validateNowAddress, trigger: "blur" }],
    studentHomeAddress: [{ validator: validateHomeAddress, trigger: "blur" }],
    studentParentName: [{ validator: validateParentName, trigger: "blur" }],
    studentParentPhone: [{ validator: validateParentPhone, trigger: "blur" }],
    studentParentJob: [{ validator: validateParentJob, trigger: "blur" }],
  }