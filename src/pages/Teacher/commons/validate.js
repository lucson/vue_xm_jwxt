const validateId = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("学号不能为空"));
  }
  callback();
};
const validateName = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("姓名不能为空"));
  } else if (value.length <= 1) {
    return callback(new Error("请填写正确的名字"));
  }
  callback();
};
const validatePhone = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("电话不能为空"));
  } else if (value.length !== 11) {
    return callback(new Error("请填写正确的电话号"));
  }
  callback();
};

const validateEmail = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("邮箱不能为空"));
  } else if (
    !/^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/.test(value)
  ) {
    return callback(new Error("请填写正确的邮箱号"));
  }
  callback();
};

const validateSalary = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("薪资不能为空"));
  }
  callback();
};

const validateSex = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("性别不能为空"));
  }
  callback();
};

const validateBirthday = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("生日不能为空"));
  }
  callback();
};
const validateExperience = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("经验不能为空"));
  }
  callback();
};
const validateInSchoolDate = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("入学日期不能为空"));
  }
  callback();
};
const validateSubject = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("院系不能为空"));
  }
  callback();
};
const validateNowAddress = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("地址不能为空"));
  } else if (value.length < 2) {
    return callback(new Error("请填写正确的地址"));
  }
  callback();
};

const validateHomeAddress = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("家乡地址不能为空"));
  } else if (value.length < 2) {
    return callback(new Error("请填写正确的地址"));
  }
  callback();
};

const validateParentName = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("父母姓名不能为空"));
  } else if (value.length <= 1) {
    return callback(new Error("请填写正确的姓名"));
  }
  callback();
};
const validateParentPhone = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("父母电话不能为空"));
  } else if (value.length !== 11) {
    return callback(new Error("请填写正确的电话号"));
  }
  callback();
};
const validateParentJob = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("父母职业不能为空"));
  } else if (value.length <= 2) {
    return callback(new Error("请填写正确的职业"));
  }
  callback();
};


export default  {
    teacherNo: [{ validator: validateId, trigger: "blur" }],
    teacherName: [{ validator: validateName, trigger: "blur" }],
    teacherPhone: [{ validator: validatePhone, trigger: "blur" }],
    teacherEmail: [{ validator: validateEmail, trigger: "blur" }],
    teacherSalary: [{ validator: validateSalary, trigger: "blur" }],
    teacherSex: [{ validator: validateSex, trigger: "blur" }],
    teacherBirthday: [{ validator: validateBirthday, trigger: "blur" }],
    teacherExperience: [{ validator: validateExperience, trigger: "blur" }],
    teacherInSchoolDate: [{ validator: validateInSchoolDate, trigger: "blur" }],
    // subject: [{ validator: validateSubject, trigger: "blur" }],
    teacherNowAddress: [{ validator: validateNowAddress, trigger: "blur" }],
    teacherHomeAddress: [{ validator: validateHomeAddress, trigger: "blur" }],
    // parentName: [{ validator: validateParentName, trigger: "blur" }],
    // parentPhone: [{ validator: validateParentPhone, trigger: "blur" }],
    // parentJob: [{ validator: validateParentJob, trigger: "blur" }],
  }