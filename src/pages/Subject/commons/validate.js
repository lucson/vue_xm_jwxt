const validateSubjectId = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("院系编号不能为空"));
  }
  callback();
};
const validateSubjectName = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("院系名称不能为空"));
  } else if (value.length <= 1) {
    return callback(new Error("请填写正确的名字"));
  }
  callback();
};
const validateSubjectClass = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("院长不能为空"));
  } else if (value.length <= 1) {
    return callback(new Error("请填写正确的院长名称"));
  }
  callback();
};


export default  {
    id: [{ validator: validateSubjectId, trigger: "blur" }],
    subjectName: [{ validator: validateSubjectName, trigger: "blur" }],
    subjectClass: [{ validator: validateSubjectClass, trigger: "blur" }],
  }