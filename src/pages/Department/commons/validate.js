const validateDepartmentId = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("院系编号不能为空"));
  }
  callback();
};
const validateDepartmentName = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("院系名称不能为空"));
  } else if (value.length <= 1) {
    return callback(new Error("请填写正确的名字"));
  }
  callback();
};
const validateDepartmentMan = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("院长不能为空"));
  } else if (value.length <= 1) {
    return callback(new Error("请填写正确的院长名称"));
  }
  callback();
};

const validateDepartmentCreateDate = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("创建日期不能为空"));
  }
  callback();
};

const validateDepartmentStudentNumber = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("学生数量不能为空"));
  }
  callback();
};


export default  {
    id: [{ validator: validateDepartmentId, trigger: "blur" }],
    departmentName: [{ validator: validateDepartmentName, trigger: "blur" }],
    departmentMan: [{ validator: validateDepartmentMan, trigger: "blur" }],
    departmentCreateDate: [{ validator: validateDepartmentCreateDate, trigger: "blur" }],
    studentNumber: [{ validator: validateDepartmentStudentNumber, trigger: "blur" }],
  }