const validatetradeStudentId = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("交易对象不能为空"));
  }
  callback();
};
const validateTradeType = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("交易类型不能为空"));
  } 
  callback();
};

const validateTradeDesc = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("交易描述不能为空"));
  }
  callback();
};

const validateTradePrice = (rule, value, callback) => {
  if (!value) {
    return callback(new Error("交易价格不能为空"));
  }
  callback();
};


export default  {
  toId: [{ validator: validatetradeStudentId, trigger: "blur" }],
  tradeType: [{ validator: validateTradeType, trigger: "blur" }],
  tradeDesc: [{ validator: validateTradeDesc, trigger: "blur" }],
  tradeMoney: [{ validator: validateTradePrice, trigger: "blur" }],
  }