import CustomTag from "@/components/CustomTag"

// 模拟一个自己写组件给别人用的场景

export default {
     install(Vue){
          Vue.component(CustomTag.name,CustomTag);
     }
}